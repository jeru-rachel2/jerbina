﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CubeWizard;

namespace Tester
{
    class BlTests
    {
        static void Main(string[] args)
        {
            // ctor for bl
            Bl myBl = new Bl();
            // return cube and query
            //Console.WriteLine(myBl.GetMetaDataByQureyID("1").queryString);
            //CubeAndQuery myCube = new CubeAndQuery();
            
            // test mdxres directly
            //myCube.cubeName = "JER_SLA";
            //myCube.queryString = "SELECT NON EMPTY { [Measures].[Count_Last_Status] } ON COLUMNS," + "NON EMPTY { ([Create Date].[YearMonthDay].[Day Of Month].ALLMEMBERS * " + "[Dim Request Type].[Request Form Type ID].[Request Form Type ID].ALLMEMBERS ) } " + " ON ROWS FROM [JERU DWH]";
            // var res = myBl.GetMdxRes(myCube);

            // get the result of a query by id
            //var resu = myBl.GetResByQueryID("1");
            //RowColStringLists tryThis = myBl.ReturnMe<RowColStringLists>("lala", Helpers.ResultType.Json);
            var resu1 = myBl.GetResByQueryName("test_worker");
            // trying to find generic return type for the same function
            // that is not highly expensive in cpu
            //string tryThis = myBl.ReturnMe<string>("lala", Helpers.ResultType.Json);
            //Bl myBlNew = new Bl();
            //string chilki = myBlNew.TryGetResByQueryID("1");

            Console.ReadKey();
        }

     }
}
