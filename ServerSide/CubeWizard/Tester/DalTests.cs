﻿using System;
using CubeWizard;
using System.Data;
using System.Collections.Generic;

namespace Tester
{
    class DalTests
    {
        static void Main(string[] args)
        {
            // init params
            string res = string.Empty;
            string mdxData = string.Empty;
            string sqlData = string.Empty;
            // mdx query sample
            string mdxQuery = 
                @"SELECT 
                    NON EMPTY { [Measures].[Count_Last_Status] } ON COLUMNS,
                    NON EMPTY { ([Create Date].[YearMonthDay].[Day Of Month].ALLMEMBERS * 
                    [Dim Request Type].[Request Form Type ID].[Request Form Type ID].ALLMEMBERS ) } 
                    ON ROWS 
                FROM [JERU DWH]";

            string sqlQuery = "SELECT * FROM Mng_BiQuery";

            // check mdx query 
            // initialize by connection string name
            MdxClient myCube = new MdxClient("JER_SLA");
            // get result and convert it to json using my builtin helper
            mdxData = myCube.GetMdxRes(mdxQuery).ResToJson();
            Console.WriteLine("Mdx Data {0}", mdxData);
            // check coonversion between datatable to datarow
            // don't like the iterations so below you
            // can see list of objects representing datarow
            // List<DataRow> mdxResList = new List<DataRow>();
            // mdxResList = Helpers.GetDataRowsList(myCube.GetMdxDataTableResults(mdxQuery));
            RowColStringLists resList = myCube.GetMdxRes(mdxQuery)
                .ResToLists();

            // check sql query
            // initialize by connection string name
            SqlClient mySqlClient = new SqlClient("JERU_DWH");
            // convert the result to json
            sqlData = mySqlClient.GetDirectSqlResults(sqlQuery)
                .ResToJsonRows();
            Console.WriteLine("Sql Data Result: {0}", sqlData);
            
            // check stored
            Dictionary<string, string> mySpParams = new Dictionary<string, string>();
            
            mySpParams.Add("@QueryID", "1");
            // mySpOutParams.Add("@CubeName", SqlDbType.VarChar);
            // mySpOutParams.Add("@MdxQuery", SqlDbType.VarChar);
            Console.WriteLine("dataset from stored: {0}",
                mySqlClient.GetSpSqlResults("SP_GetMdxQueryCubeByID", mySpParams).ResToJson());
            // test simpler
            //mySpOutParams.Add("@bilki", SqlDbType.Int);

            Console.WriteLine("dataset from stored: {0}",
                mySqlClient.GetSpSqlResults("SP_zz_test", null).ResToJson());

            // check field value by column
            Console.WriteLine("value by column: {0}", mySqlClient.GetSpSqlResults("SP_GetMdxQueryCubeByID", mySpParams).ResByColName("BiCubeName"));

            // check sql stored
            //List<string[]> myPramas = mySqlClient.GetProcParams("SP_GetMdxQueryByID");
            //foreach (string[] param in myPramas)
            //{
            //    Console.WriteLine(param);
            //}


            // check connection strings
            Console.WriteLine("JERU_DWH".GetConStrByName().ToString());
            Console.WriteLine("JER_SLA".GetConStrByName().ToString());

            // wait for user interaction to close app
            Console.ReadKey();
        }
    }
}
