﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace CubeApi

{

    public class CubeHub : Hub
    {
        private const string HubName = "CubeHub";
        // todo: add authentication
        /// <summary>
        /// server implementation to push messages to the clients
        /// </summary>
        /// <param name="hubName">the name of the hub all clients connect to</param>
        /// <param name="groupName">the name of the sender</param>
        /// <param name="message">the message sent to the hub</param>
        public void Send(string groupName, string message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext(HubName);
            //var context = GlobalHost.ConnectionManager.GetHubContext(hubName);
            context.Clients.All.BroadcastMessage(groupName, message);
        }

    }

}


