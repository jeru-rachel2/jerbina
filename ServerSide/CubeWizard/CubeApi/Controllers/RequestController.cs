﻿using System.Web.Http;
using CubeApi.Models;
using CubeWizard;
using System.Web.Http.Cors;
using System.Threading;
using System;
//using System.Net.Http;
//using System.Net;

// todo: need to remove afer test with rachel
//using System.Collections.Generic;

namespace CubeApi.Controllers
{
    // this decoration allows cors for the resouce 
    // ignore http or https and servername
    //[EnableCors(origins: "api/v1.0" , headers: "*", methods: "*")]
    [EnableCors(origins: "*" , headers: "*", methods: "*")]
    public class RequestController : ApiController
    {
        // doing the route excplicitly overrides 
        // webapiconfig and makes it simpler like flask :)

        // get string objects of the data containing
        // column and row data
        [HttpGet]
        [Route("api/v1.0/get_cube_res/to_object/{query}/{param}")]
        public RowColStringLists GetMdxResByQueryID(string query, string param)
        {
            Bl myBl = new Bl();

            return myBl.GetResByQueryID(query); 
        }


        // get string objects of the data containing
        // column and row data by query name param is optional
        //[HttpGet]
        [HttpGet]
        [Route("api/v1.0/get_res_query_name/to_object/{queryName}/{param?}")]
        public RowColStringLists GetMdxResByQueryName(string queryName, string param = null)
        {
            Bl myBl = new Bl();
            
            return myBl.GetResByQueryName(queryName, param);
        }


        // try
        [HttpPost]
        [Route("api/v2.0/get_res_query_name/to_object/{queryName}")]
        public RowColStringLists GetMdxResByQueryNamePostV2(string queryName, [FromBody]Newtonsoft.Json.Linq.JObject param)
        {
            Bl myBl = new Bl();
            //string paramObj = param["id"].ToObject<string>();
            //RowColStringLists r = new RowColStringLists();
            //r.columnData = new List<string>();
            //r.columnData.Add(temp);
            //var chil = myBl.GetResByQueryName(queryName, temp);
            //return myBl.GetResByQueryName(queryName, temp);
            //return r;
            string paramStrJson = param.ToString();
            return myBl.GetResByQueryName(queryName, paramStrJson);
        }


        // get string objects of the data containing
        // column and row data by query name param is optional
        //[HttpGet]
        [HttpPost]
        [Route("api/v1.0/get_res_query_name/to_object/{queryName}")]
        public RowColStringLists GetMdxResByQueryNamePost(string queryName, [FromBody]Newtonsoft.Json.Linq.JObject param)
        {
            Bl myBl = new Bl();
            string paramObj = param["id"].ToObject<string>();
            //RowColStringLists r = new RowColStringLists();
            //r.columnData = new List<string>();
            //r.columnData.Add(temp);
            //var chil = myBl.GetResByQueryName(queryName, temp);
            //return myBl.GetResByQueryName(queryName, temp);
            //return r;
            return myBl.GetResByQueryName(queryName, paramObj);
        }


        // get string objects of the data containing
        // column and row data by query name param is optional
        [HttpGet]
        [Route("api/v1.0/send_message/{groupName}/{message}")]
        public string SendMessageToClients(string groupName, string message)
        {
            CubeHub myHub = new CubeHub();
            
            myHub.Send(groupName, message);

            return "Message Sent";
        }



        //// get json data containing
        //// column and row data
        //[HttpGet]
        //[Route("api/v1.0/get_cube_res/to_json/{query}/{param}")]
        //public RowColStringLists GetMdxRes(string query, string param)
        //{
        //    Bl myBl = new Bl();

        //    return myBl.GetResByQueryID(query);
        //}

        [HttpGet]
        [Route("api/v1.0/test")]
        public Response Requests()
        {
            Response reqResponse = new Response();
            // get user identity
            reqResponse.MetaData = RequestContext.Principal.Identity.Name;
            // return results

            reqResponse.Results = "Chilki Bilki";

            return reqResponse;
        }

        //trying to find user
        [HttpGet]
        [Route("")]
        public string RequestUser()
        {
            // get user identity
            string username = string.Empty;
            
            username = System.Web.HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrEmpty(username))
                username = Environment.UserName;
            if (string.IsNullOrEmpty(username))
                username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if (username.Contains("KIKARDOM"))
                username = (username.Substring(username.LastIndexOf("\\") + 1)).ToLower();
            
            return username;
        }


        /*

        [HttpGet]
        public Response Request(int queryId, string[] param, string returnType)
        {
            Response reqResponse = new Response();
            reqResponse.MetaData = queryId.ToString() + returnType;
            reqResponse.Results = "Chilki Bilki";

            return reqResponse;
        }
        */
    }
}
