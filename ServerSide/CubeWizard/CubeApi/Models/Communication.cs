﻿// Entities for CubeWizard
namespace CubeApi.Models
{
    public class request
    {
        public int QueryID { get; set; }
        public string[] Params { get; set; }
        public int ReturnType { get; set; }
    }

    public class Response
    {
        public string MetaData { get; set; }
        public string Results { get; set; }
    }
}