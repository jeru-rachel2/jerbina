﻿using System;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
// to add refernce work according to: 
// @"https://docs.microsoft.com/en-us/sql/analysis-services/
//         tabular-model-programming-compatibility-level-1200/
//         install-distribute-and-reference-the-tabular-object-model"
// the object was found in the gac named: Microsoft.AnalysisServices.AdomdClient.dll
using Microsoft.AnalysisServices.AdomdClient;


namespace CubeWizard
{

    // connects to cube in order to return queries
    public class MdxClient
    {
        private string conStr = string.Empty;
        private AdomdConnection adomdCon;
        
        public MdxClient(string connectionName)
        {
            // create the connection string for mdx
            this.conStr = connectionName.GetConStrByName();
            // create and open adomd connection with connection string
            this.adomdCon = new AdomdConnection(this.conStr);
            this.adomdCon.Open();
            //try
            //{
            //    this.adomdCon.Open();
            //}
            //catch (Exception e)
            //{
            //    // todo: implement
            //    throw e;
            //}
        }
        
        //
        public CellSet GetMdxCellSetRes(string query)
        {
            // create adomd command using connection and MDX query
            AdomdCommand cmd = new AdomdCommand(query, this.adomdCon);
            // retrun ssas cell set
            CellSet cst = cmd.ExecuteCellSet();

            return cst;
        }
        
        // return basic result from mdx
        public DataTable GetMdxRes(string query)
        {
            string readerString = string.Empty;
            List<string> curColumn = new List<string>();
            string column = string.Empty;
            DataTable dataTable = new DataTable();
            try
            {
                // create adomd command using connection and MDX query
                AdomdCommand cmd = new AdomdCommand(query.ToString(), adomdCon);

                //fill the data adapter
                AdomdDataAdapter da = new AdomdDataAdapter(cmd);
                // fill the datatable

                da.Fill(dataTable);

            }
            // error retreiving from adomd or from adomdadapter
            catch (Exception e)
            {
                // todo: implement e handling through tiers
                throw e;
            }
                

            // iterate so data will be human readable (removing mdx tags and symbols)
            if (dataTable.Rows.Count > 0 && dataTable.Columns.Count > 0)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    string columnName = dataTable.Columns[i].ColumnName.Replace(".[MEMBER_CAPTION]",
                        "").Trim();
                    curColumn = columnName.Split(new string[] {"."},
                        StringSplitOptions.None).ToList();
                    column = curColumn[curColumn.Count - 1].Replace("[", "").Replace("]", "");
                    dataTable.Columns[i].ColumnName = column;
                }
                // update datatable with modifications made
                dataTable.AcceptChanges();
            }
            
            return dataTable;
        }

    }


    // deals with sql connections
    // mainly for metadata extraction
    // and logging
    public class SqlClient
    {
        private string conStr = string.Empty;
        private SqlConnection sqlCon;
        public SqlClient(string connenctionName)
        {
            // create the connection string for sql
            this.conStr = connenctionName.GetConStrByName();
            // create and open sql connection with connection string
            this.sqlCon = new SqlConnection(this.conStr);
            this.sqlCon.Open();
        }

        // get tsql results from direct query
        // for internal use only!!!
        // todo: handle sanitization to prevent sql injection?
        public DataTable GetDirectSqlResults(string query)
        {
            SqlDataReader sqlReader;
            SqlCommand sqlCmd;
            DataTable dataTable = new DataTable();

            // prepare sql cmd with connection 
            sqlCmd = new SqlCommand(query, this.sqlCon);
            // execute the query into sql data reader
            sqlReader = sqlCmd.ExecuteReader();
            if (sqlReader.HasRows)
            {
                // fill the datatable
                dataTable.Load(sqlReader);
            }

            return dataTable;
        }

        // return sql results from sp
        // this should be the default way to get data
        // this is instead of EF implementation due to performance
        // simplcity and maintainability - 
        // todo: need to close connection after performed make it static
        public DataTable GetSpSqlResults(string procName ,Dictionary<string, string> inParam = null)
        {
            SqlCommand sqlCmd;
            SqlDataReader sqlReader;
            //List<string[]> result = new List<string[]>();
            DataTable dataTable = new DataTable();
            //SqlParameter sqlParam = new SqlParameter();

            sqlCmd = new SqlCommand(procName, this.sqlCon);
            // set this command to stored
            sqlCmd.CommandType = CommandType.StoredProcedure;

            // insert in params
            if (inParam != null)
            {
                foreach (var pair in inParam)
                {
                    sqlCmd.Parameters.AddWithValue(pair.Key, pair.Value).Direction
                        = System.Data.ParameterDirection.Input;
                }
            }

            sqlReader = sqlCmd.ExecuteReader();
            if (sqlReader.HasRows)
            {
                // fill the datatable
                dataTable.Load(sqlReader);
            }
            sqlReader.Close();

            return dataTable;
        }
       

    }

    
    // retrieve connection strings
    // from consuming application app.config
    public static class DataConfig
    {

        public static string GetConStrByName(this string name)
        {
            // just in case
            string returnValue = string.Empty;

            // look for the name in the connectionStrings section
            ConnectionStringSettings settings =
                ConfigurationManager.ConnectionStrings[name];

            // if found, return the connection string
            if (settings != null)
                returnValue = settings.ConnectionString;
            else
            {
                throw new
                    System.InvalidOperationException("Config file or property cannot be read");
            }

            return returnValue;
        }

    }
}
