﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace CubeWizard
{
    /// <summary>
    /// deals with query request types
    /// converts them to mdx by the dal, queries 
    /// them by the dal and return the results
    /// </summary>
    public class Bl
    {
        // client for sql
        protected SqlClient mySqlClient;

        // parameters for stored 
        private Dictionary<string, string> mySpParams;

        // ctor for sql metadata that is required
        // prior to any query 
        public Bl()
        {
            // initialize by connection string name for metadata 
            // required for mdx queries, cubes and etc.
            mySpParams = new Dictionary<string, string>();

            mySqlClient = new SqlClient(Consts.MetaDataConn);
        }


        // manage the request to get data
        // get metadata and pass for query execution by query id
        public RowColStringLists GetResByQueryID(string queryID)
        {
            CubeAndQuery cubeQuery = new CubeAndQuery();

            // i don't return the set to see if it's empty first
            cubeQuery = GetMetaDataByQureyID(queryID);
            // handle empty set
            if (string.IsNullOrEmpty(cubeQuery.cubeName) || string.IsNullOrEmpty(cubeQuery.queryString))
                return new RowColStringLists();

            return GetMdxRes(cubeQuery);
        }

        // manage the request to get data
        // get metadata and pass for query execution by query name
        public RowColStringLists GetResByQueryName(string queryName, string param = null)
        {
            CubeAndQuery cubeQuery = new CubeAndQuery();

            // handle queries with parameters
            if (!string.IsNullOrEmpty(param))
                cubeQuery = GetMetaDataByQureyName(queryName, param);
            else
                cubeQuery = GetMetaDataByQureyName(queryName);
            // handle empty set
            if (string.IsNullOrEmpty(cubeQuery.cubeName) || string.IsNullOrEmpty(cubeQuery.queryString))
                return new RowColStringLists();

            return GetMdxRes(cubeQuery);
        }


        // return cube name and mdx query string by query id
        protected CubeAndQuery GetMetaDataByQureyID(string queryID)
        {
            CubeAndQuery metaData = new CubeAndQuery();
            // return result from mdx query
            DataTable myData = new DataTable();

            // add query id as input param
            mySpParams.Add("@QueryID", queryID);
            // return results to datatable structure
            myData = mySqlClient.GetSpSqlResults("SP_GetMdxQueryCubeByID", mySpParams);
            // get values for query and cube
            metaData.cubeName = myData.ResByColName("BiCubeName");
            metaData.queryString = myData.ResByColName("BiQueryMdx");

            return metaData;
        }

        // return cube name and mdx query string by query name
        protected CubeAndQuery GetMetaDataByQureyName(string queryName)
        {
            CubeAndQuery metaData = new CubeAndQuery();
            // return result from mdx query
            DataTable myData = new DataTable();

            // add query id as input param
            mySpParams.Add("@QueryName", queryName);
            // return results to datatable structure
            myData = mySqlClient.GetSpSqlResults("SP_GetMdxQueryCubeByName", mySpParams);
            // get values for query and cube
            metaData.cubeName = myData.ResByColName("BiCubeName");
            metaData.queryString = myData.ResByColName("BiQueryMdx");

            return metaData;
        }

        // return cube name and mdx query string by query name and params
        protected CubeAndQuery GetMetaDataByQureyName(string queryName, string param)
        {
            CubeAndQuery metaData = new CubeAndQuery();
            try
            {
                // return result from mdx query
                DataTable myData = new DataTable();

                // add query id as input param
                mySpParams.Add("@QueryName", queryName);
                mySpParams.Add("@QueryParam", param);
                // return results to datatable structure
                myData = mySqlClient.GetSpSqlResults("SP_GetMdxQueryCubeByNameAndParams", mySpParams);
                // get values for query and cube
                metaData.cubeName = myData.ResByColName("BiCubeName");
                metaData.queryString = myData.ResByColName("BiQueryMdx");

            }
            catch (Exception ex)
            {

            }
            return metaData;
        }


        // return mdx result as two string list sets
        protected RowColStringLists GetMdxRes(CubeAndQuery metaData, string param = null)
        {
            MdxClient myCube = new MdxClient(metaData.cubeName);
            // todo: implement drill in dal and in here

            return myCube.GetMdxRes(metaData.queryString).ResToLists();
        }


        #region try generalize
        // trying to find generic return type for the same function
        // that is not highly expensive in cpu
        public T ReturnMe<T>(string key, Helpers.ResultType resultType) where T : IConvertible
        {
            if (resultType == Helpers.ResultType.ColumnRow)
                return (T)Convert.ChangeType(new RowColStringLists(), typeof(T), CultureInfo.InvariantCulture);
            else
                return (T)Convert.ChangeType("chilki", typeof(T), CultureInfo.InvariantCulture);
        }


        // manage the request to get data
        // get metadata and pass for query execution
        public string TryGetResByQueryID(string queryID)
        {
            CubeAndQuery cubeQuery = new CubeAndQuery();

            cubeQuery = GetMetaDataByQureyID(queryID);
            if (string.IsNullOrEmpty(cubeQuery.cubeName) || string.IsNullOrEmpty(cubeQuery.queryString))
                return "";

            return TryGetMdxRes(cubeQuery);
        }

        // return mdx result as two string list sets
        protected string TryGetMdxRes(CubeAndQuery metaData, string param = null)
        {
            MdxClient myCube = new MdxClient(metaData.cubeName);
            // todo: implement drill in dal and in here

            return myCube.GetMdxRes(metaData.queryString).ResToLists().ToString();
        }
        #endregion




    }

}
