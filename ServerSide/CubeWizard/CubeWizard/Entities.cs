﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections.Generic;


namespace CubeWizard
{
    // return result set with metadata
    public struct RowColStringLists
    {
        public List<string> columnData;
        public List<string[]> rowData;
    }

    // return cube name and query 
    // string
    public struct CubeAndQuery
    {
        public string cubeName;
        public string queryString;
    }
}
