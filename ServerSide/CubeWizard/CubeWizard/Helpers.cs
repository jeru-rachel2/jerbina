﻿using System.Data;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CubeWizard
{
    /// <summary>
    /// help in data conversion
    /// and more as extension methods
    /// </summary>
    public static class Helpers
    {
        // result type for generic function 
        // that return data by result type
        // ResultWithMetaDataToString
        public enum ResultType  { Json, ColumnRow };
        // get datatable return json

        public static string ResToJson(this DataTable data)
        {
            string rows, columns = string.Empty;

            rows = data.ResToJsonRows();
            columns = data.ResToJsonCol();

            return columns + rows;
        }

        public static string ResToJsonRows(this DataTable data)
        {
            return JsonConvert.SerializeObject(new { Rows = data } );
        }

        // return 2 jsons: one for column
        // the other with the rows

        private static string ResToJsonCol(this DataTable data)
        {
            return JsonConvert.SerializeObject(new { Columns = data.ResToColStrLst() });
        }

        // Todo: need to finalize
        // return datacolumn and datarows 
        public static RowColStringLists ResToLists(this DataTable data)
        {
            RowColStringLists rows = new RowColStringLists();

            rows.rowData = data.ResToRowStrLst();
            rows.columnData = data.ResToColStrLst();

            return rows;
        }
        
        // todo: implement
        //public static string ResToStr(this DataTable data, ResultType res)
        //{

            
        //    return "";
        //}

        // returns list of strings
        // representing column data
        // meaning query metadata
        public static List<string> ResToColStrLst(this DataTable data)
        {
            List<string> column = 
                data.Columns.Cast<DataColumn>()
                    .Select(x => 
                        x.ColumnName.ToString())
                        .ToArray()
                    .ToList();

            return column;
        }

        // return list of arrays of strings
        // composing the row data
        public static List<string[]> ResToRowStrLst(this DataTable data)
        {

            List <string[]> results =
                data.Select()
                    .Select(dr =>
                        dr.ItemArray
                            .Select(x => x.ToString())
                            .ToArray())
                    .ToList();

            return results;
        }

        // return list of arrays of strings
        // composing the row data
        public static string ResByColName(this DataTable data, string colName)
        {
            if (data.Rows.Count > 0 )
            {
                return data.Rows[0][colName].ToString();
            }

            return string.Empty;
        }

    }

}
