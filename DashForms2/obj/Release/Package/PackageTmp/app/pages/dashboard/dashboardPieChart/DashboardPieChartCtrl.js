/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.dashboard')
        .controller('DashboardPieChartCtrl', DashboardPieChartCtrl);

    /** @ngInject */
    function DashboardPieChartCtrl($scope, $timeout, baConfig, baUtil,Translate) {
        $(function () {
            $('#id0').highcharts({
                title: '',
                chart: {
                    type: 'solidgauge'
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                    },
                    positioner: function (labelWidth) {
                        return {
                            x: 200 - labelWidth / 2,
                            y: 180
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{ // Track for Move
                        outerRadius: '112%',
                        innerRadius: '88%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[8]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    gridLineColor: 'black',
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return '';
                        }
                    },
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    name: '',
                    borderColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                    data: [{
                        color: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                        radius: '100%',
                        innerRadius: '100%',
                        y: 79
                    }]
                }],
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickWidth: 0,
                    categories: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '9px',
                        dataLabels: {
                            enabled: true,
                            y: -15,
                            x: 0,
                            format: '<span style=" font-weight:bold">' + 79 + '%</span>',
                            style: {
                                color: 'black',
                                fontSize: '20px',
                                fontWeight: 'normal',
                                fontStyle: 'normal',
                                fontFamily: 'Arial',
                            },
                            labels: {
                                style: {
                                    fontSize: '21px',
                                    fontWeight: 'normal',
                                    fontStyle: 'normal',
                                    fontFamily: 'Arial',
                                }
                            },
                        },
                        stickyTracking: false
                    }
                },



            });
            $('#id1').highcharts({
                title: '',
                chart: {
                    type: 'solidgauge'
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                    },
                    positioner: function (labelWidth) {
                        return {
                            x: 200 - labelWidth / 2,
                            y: 180
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{ // Track for Move
                        outerRadius: '112%',
                        innerRadius: '88%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[8]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    gridLineColor: 'black',
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return '';
                        }
                    },
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    name: '',
                    borderColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                    data: [{
                        color: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                        radius: '100%',
                        innerRadius: '100%',
                        y: 79
                    }]
                }],
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickWidth: 0,
                    categories: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '9px',
                        dataLabels: {
                            enabled: true,
                            y: -15,
                            x: 0,
                            format: '<span style=" font-weight:bold">' + 79 + '%</span>',
                            style: {
                                color: 'black',
                                fontSize: '20px',
                                fontWeight: 'normal',
                                fontStyle: 'normal',
                                fontFamily: 'Arial',
                            },
                            labels: {
                                style: {
                                    fontSize: '21px',
                                    fontWeight: 'normal',
                                    fontStyle: 'normal',
                                    fontFamily: 'Arial',
                                }
                            },
                        },
                        stickyTracking: false
                    }
                },



            });
            $('#id2').highcharts({
                title: '',
                chart: {
                    type: 'solidgauge'
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                    },
                    positioner: function (labelWidth) {
                        return {
                            x: 200 - labelWidth / 2,
                            y: 180
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{ // Track for Move
                        outerRadius: '112%',
                        innerRadius: '88%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[8]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    gridLineColor: 'black',
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return '';
                        }
                    },
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    name: '',
                    borderColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                    data: [{
                        color: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                        radius: '100%',
                        innerRadius: '100%',
                        y: 79
                    }]
                }],
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickWidth: 0,
                    categories: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '9px',
                        dataLabels: {
                            enabled: true,
                            y: -15,
                            x: 0,
                            format: '<span style=" font-weight:bold">' + 79 + '%</span>',
                            style: {
                                color: 'black',
                                fontSize: '20px',
                                fontWeight: 'normal',
                                fontStyle: 'normal',
                                fontFamily: 'Arial',
                            },
                            labels: {
                                style: {
                                    fontSize: '21px',
                                    fontWeight: 'normal',
                                    fontStyle: 'normal',
                                    fontFamily: 'Arial',
                                }
                            },
                        },
                        stickyTracking: false
                    }
                },



            });
            $('#id3').highcharts({
                title: '',
                chart: {
                    type: 'solidgauge'
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                    },
                    positioner: function (labelWidth) {
                        return {
                            x: 200 - labelWidth / 2,
                            y: 180
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{ // Track for Move
                        outerRadius: '112%',
                        innerRadius: '88%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[8]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    gridLineColor: 'black',
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return '';
                        }
                    },
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    name: '',
                    borderColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                    data: [{
                        color: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                        radius: '100%',
                        innerRadius: '100%',
                        y: 79
                    }]
                }],
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickWidth: 0,
                    categories: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '9px',
                        dataLabels: {
                            enabled: true,
                            y: -15,
                            x: 0,
                            format: '<span style=" font-weight:bold">' + 79 + '%</span>',
                            style: {
                                color: 'black',
                                fontSize: '20px',
                                fontWeight: 'normal',
                                fontStyle: 'normal',
                                fontFamily: 'Arial',
                            },
                            labels: {
                                style: {
                                    fontSize: '21px',
                                    fontWeight: 'normal',
                                    fontStyle: 'normal',
                                    fontFamily: 'Arial',
                                }
                            },
                        },
                        stickyTracking: false
                    }
                },



            });
            $('#id4').highcharts({
                title: '',
                chart: {
                    type: 'solidgauge'
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                    },
                    positioner: function (labelWidth) {
                        return {
                            x: 200 - labelWidth / 2,
                            y: 180
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{ // Track for Move
                        outerRadius: '112%',
                        innerRadius: '88%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[8]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    gridLineColor: 'black',
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return '';
                        }
                    },
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    name: '',
                    borderColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                    data: [{
                        color: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                        radius: '100%',
                        innerRadius: '100%',
                        y: 79
                    }]
                }],
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickWidth: 0,
                    categories: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '9px',
                        dataLabels: {
                            enabled: true,
                            y: -15,
                            x: 0,
                            format: '<span style=" font-weight:bold">' + 79 + '%</span>',
                            style: {
                                color: 'black',
                                fontSize: '20px',
                                fontWeight: 'normal',
                                fontStyle: 'normal',
                                fontFamily: 'Arial',
                            },
                            labels: {
                                style: {
                                    fontSize: '21px',
                                    fontWeight: 'normal',
                                    fontStyle: 'normal',
                                    fontFamily: 'Arial',
                                }
                            },
                        },
                        stickyTracking: false
                    }
                },



            });
            $('#id5').highcharts({
                title: '',
                chart: {
                    type: 'solidgauge'
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                    },
                    positioner: function (labelWidth) {
                        return {
                            x: 200 - labelWidth / 2,
                            y: 180
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{ // Track for Move
                        outerRadius: '112%',
                        innerRadius: '88%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[8]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    gridLineColor: 'black',
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return '';
                        }
                    },
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    name: '',
                    borderColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                    data: [{
                        color: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.7).get(),
                        radius: '100%',
                        innerRadius: '100%',
                        y: 79
                    }]
                }],
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickWidth: 0,
                    categories: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '9px',
                        dataLabels: {
                            enabled: true,
                            y: -15,
                            x: 0,
                            format: '<span style=" font-weight:bold">' + 79 + '%</span>',
                            style: {
                                color: 'black',
                                fontSize: '20px',
                                fontWeight: 'normal',
                                fontStyle: 'normal',
                                fontFamily: 'Arial',
                            },
                            labels: {
                                style: {
                                    fontSize: '21px',
                                    fontWeight: 'normal',
                                    fontStyle: 'normal',
                                    fontFamily: 'Arial',
                                }
                            },
                        },
                        stickyTracking: false
                    }
                },



            });
        });


        var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
        $scope.charts = [{
            datapercent: 71,
            color: pieColor,
            description: Translate('SubOpenCloseREquest'),
            //stats: '178,391',
            icon: 'person',
        }, {
            datapercent: 75,
            color: pieColor,
            description: Translate('SumRequestOnTime'),
            //stats: '57,820',
            icon: 'face',
        }, {
            datapercent: 88,
            color: pieColor,
            description: Translate('MyRequestsOpenCloseCount'),
            //stats: '$ 89,745',
            icon: 'face',
        }, {
            datapercent: 84,
            color: pieColor,
            description: Translate('SumRequestToResident2'),
            //stats: '178,391',
            icon: 'info',
        }
        ];

        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }

        function loadPieCharts() {
            $('.chart').each(function () {
                var chart = $(this);
                chart.easyPieChart({
                    easing: 'easeOutBounce',
                    onStep: function (from, to, percent) { 
                        $(this.el).find('.percent').text(Math.round(to));
                        //$(this.el).find('.percent').text(Math.round(percent));
                    },
                    barColor: chart.attr('rel'),
                    trackColor: 'rgba(0,0,0,0)',
                    size: 84,
                    scaleLength: 0,
                    animation: 2000,
                    lineWidth: 9,
                    lineCap: 'round',
                });
            });

            $('.refresh-data').on('click', function () {
                updatePieCharts();
            });
        }

        function updatePieCharts() {
            $('.pie-charts .chart').each(function (index, chart) {
                $(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
            });
        }

        $timeout(function () {
            loadPieCharts();
            updatePieCharts();
        }, 1000);
    }
})();