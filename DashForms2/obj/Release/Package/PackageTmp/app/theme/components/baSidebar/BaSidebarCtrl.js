/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme.components')
      .controller('BaSidebarCtrl', BaSidebarCtrl);

    /** @ngInject */
    function BaSidebarCtrl(IdmService, $scope, baSidebarService, Translate, LoginService) {
        var vm = this;
        vm.user = {};
        $scope.data = {};
        var WinNetWork = new ActiveXObject("WScript.Network");
        vm.user.userName = WinNetWork.UserName;
        LoginService.get({ id: WinNetWork.UserName }, function (data) {
            //data.user_groups='42001'
            if (data.user_groups == '42001' || data.user_groups == '4200201')
            $scope.Idm = data.user_groups;
            $scope.data.selected = data.user_groups;
            localStorage.setItem('userName', vm.user.userName);
            localStorage.setItem('display_name', data.display_name);
            localStorage.setItem('group', $scope.data.selected);
            $scope.ChangeIdm();
        });

        //*
        $scope.Per = [
           { id: '42001', Name: Translate('IDM1') },
           { id: '4200201', Name: Translate('IDM2') },
           { id: '4200202', Name: Translate('IDM3') },
           { id: '4200203', Name: Translate('IDM4') },
           { id: '4200301', Name: Translate('IDM5') },
           { id: '4200302', Name: Translate('IDM6') },
           { id: '42004', Name: Translate('IDM7') }
        ]
        //$scope.data = { selected: '42001' };
        //$scope.Idm = $scope.Per[0].id;
        //*
        $scope.ChangeIdm = function () {
            $scope.CurrentIdm = $scope.data.selected;
            IdmService.query({ id: $scope.CurrentIdm }, function (data) {
                $scope.menuItems = [];
                var ind = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].IsFather == true) {
                        $scope.menuItems[ind] = {
                            icon: "",
                            level: data[i].Id,
                            name: data[i].Description,
                            order: 0,
                            stateRef: "",
                            title: data[i].Description,
                        }
                        $scope.menuItems[ind].subMenu = [];
                        var ind2 = 0;
                        for (var j = 0; j < data.length; j++) {
                            if (data[j].FatherGroups == data[i].Id) {
                                $scope.menuItems[ind].subMenu[ind2] = {
                                    icon: "",
                                    level: i,
                                    name: data[j].Description,
                                    order: 0,
                                    stateRef: data[j].stateRef,
                                    title: data[j].Description
                                }
                                ind2++;
                            }
                        }
                        ind++;
                    }
                }

                $scope.defaultSidebarState = $scope.menuItems[0].stateRef;
            });
        }




        //$scope.menuItems[0] = {
        //    icon: "",
        //    level: 0,
        //    name: "dashboard",
        //    order: 0,
        //    stateRef: "",
        //    title: Translate("m10"),
        //    subMenu: [
        //     {
        //         icon: "",
        //         level: 1,
        //         name: "dashboard",
        //         order: 0,
        //         stateRef: "dashboard",
        //         title: Translate("Dashboard")
        //     }, {
        //         icon: "",
        //         level: 1,
        //         name: "dashboard2",
        //         order: 1,
        //         stateRef: "dashboard2",
        //         title: Translate("Dashboard2")
        //     }, {
        //         icon: "",
        //         level: 1,
        //         name: "dashboard3",
        //         order: 2,
        //         stateRef: "dashboard3",
        //         title: Translate("Dashboard3")
        //     }
        //    ]
        //}
        //$scope.menuItems[1] = {
        //    icon: "",
        //    level: 2,
        //    name: "report",
        //    order: 0,
        //    stateRef: " ",
        //    title: Translate("m"),
        //    subMenu: [
        //    {
        //        icon: "",
        //        level: 3,
        //        name: "generalReport",
        //        order: 0,
        //        stateRef: "generalReport",
        //        title: Translate("m1")
        //    }, {
        //         icon: "",
        //         level: 3,
        //         name: "report",
        //         order: 0,
        //         stateRef: "report",
        //         title: Translate("m2")
        //     }, {
        //         icon: "",
        //         level: 3,
        //         name: "report2",
        //         order: 1,
        //         stateRef: "report2",
        //         title: Translate("m3")
        //     }
        //    ]
        //}
        //$scope.menuItems[2] = {
        //    icon: "",
        //    level: 4,
        //    name: "historyInd",
        //    order: 0,
        //    stateRef: "",
        //    title: Translate("m4"),
        //    subMenu: [
        //     {
        //         icon: "",
        //         level: 5,
        //         name: "historyInd",
        //         order: 0,
        //         stateRef: "historyInd",
        //         title: Translate("m5")
        //     }, {
        //         icon: "",
        //         level: 5,
        //         name: "historyInd2",
        //         order: 1,
        //         stateRef: "historyInd2",
        //         title: Translate("m6")
        //     }
        //    ]
        //}
        //$scope.menuItems[3] = {
        //    icon: "",
        //    level: 5,
        //    name: "dataManage",
        //    order: 0,
        //    stateRef: "",
        //    title: Translate("m7"),
        //    subMenu: [
        //     {
        //         icon: "",
        //         level: 6,
        //         name: "dataManage",
        //         order: 0,
        //         stateRef: "dataManage",
        //         title: Translate("m8")
        //     }, {
        //         icon: "",
        //         level: 6,
        //         name: "dataManage2",
        //         order: 1,
        //         stateRef: "dataManage2",
        //         title: Translate("m9")
        //     }
        //    ]
        //}

        $scope.hoverItem = function ($event) {
            $scope.showHoverElem = true;
            $scope.hoverElemHeight = $event.currentTarget.clientHeight;
            var menuTopValue = 66;
            $scope.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - menuTopValue;
        };
        $scope.$on('$stateChangeSuccess', function () {
            if (baSidebarService.canSidebarBeHidden()) {
                baSidebarService.setMenuCollapsed(true);
            }
        });
    }
})();