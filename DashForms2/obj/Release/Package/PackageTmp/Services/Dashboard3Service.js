﻿angular.module('BlurAdmin.pages.charts.chartist').factory('Dashboard3Service', Dashboard3Service);
function Dashboard3Service($resource, a) {
  return $resource('/api/ApiDashboard3/:id', {
     //     return $resource('/dashboard/api/ApiDashboard3/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
Dashboard3Service.$inject = ['$resource'];

 