﻿angular.module('BlurAdmin.pages.charts.chartist').factory('HistoryIndService', HistoryIndService);
function HistoryIndService($resource, a) {
   return $resource('/api/ApiHistoryInd/:id', {
        // return $resource('/dashboard/api/ApiHistoryInd/:id', {
        id: '@id', 
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
HistoryIndService.$inject = ['$resource'];

angular.module('BlurAdmin.pages.charts.chartist').factory('HistoryIndPutService', HistoryIndPutService);
function HistoryIndPutService($resource, a) {
     return $resource('/api/ApiHistoryIndPut/:id', {
      // return $resource('/dashboard/api/ApiHistoryIndPut/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
HistoryIndPutService.$inject = ['$resource'];