﻿angular.module('BlurAdmin.pages.charts.chartist').factory('DataManage2Service', DataManageService);
function DataManageService($resource, a) {
        // return $resource('/dashboard/api/ApiDataManage2/:id/:type', {
   return $resource('/api/ApiDataManage2/:id/:type', {
        id: '@id',
        type: '@type',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DataManageService.$inject = ['$resource'];
