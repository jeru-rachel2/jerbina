﻿angular.module('BlurAdmin.pages.charts.chartist').factory('ListService', ListService);
function ListService($resource, a) {
    // return $resource('/api/ApiList/:id', {
       return $resource('/dashboard/api/ApiList/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
ListService.$inject = ['$resource'];
