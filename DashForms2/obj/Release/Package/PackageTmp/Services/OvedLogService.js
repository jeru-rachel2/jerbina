﻿angular.module('BlurAdmin.pages.charts.chartist').factory('OvedLogService', OvedLogService);
function OvedLogService($resource, a) {
       return $resource('/api/ApiOvedLog/:id', {
    //   return $resource('/dashboard/api/ApiOvedLog/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
OvedLogService.$inject = ['$resource'];