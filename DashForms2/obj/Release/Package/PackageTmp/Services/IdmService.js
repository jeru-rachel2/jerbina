﻿angular.module('BlurAdmin.pages.charts.chartist').factory('IdmService', IdmService);
function IdmService($resource, a) {
    return $resource('/api/ApiIdm/:id', {
      //  return $resource('/dashboard/api/ApiIdm/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
IdmService.$inject = ['$resource'];