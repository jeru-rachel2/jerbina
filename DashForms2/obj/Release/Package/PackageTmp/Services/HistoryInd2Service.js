﻿angular.module('BlurAdmin.pages.charts.chartist').factory('HistoryInd2Service', HistoryInd2Service);
function HistoryInd2Service($resource, a) {
    return $resource('/api/ApiHistoryInd2/:id', {
      //  return $resource('/dashboard/api/ApiHistoryInd2/:id', {
        id: '@id', 
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
HistoryInd2Service.$inject = ['$resource'];
