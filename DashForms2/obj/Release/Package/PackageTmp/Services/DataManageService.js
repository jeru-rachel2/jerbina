﻿angular.module('BlurAdmin.pages.charts.chartist').factory('DataManageService', DataManageService);
function DataManageService($resource, a) {
  return $resource('/api/ApiDataManage/:id', {
        //  return $resource('/dashboard/api/ApiDataManage/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DataManageService.$inject = ['$resource'];

angular.module('BlurAdmin.pages.charts.chartist').factory('DataManageServicePut', DataManageServicePut);
function DataManageServicePut($http) {
    var vm = this;
    vm.put = function (id, mh) {
        var Indata = { id: id, mh: mh }
     return $http.post('/api/ApiDataManagePut', Indata).success(function (response) {
             //  return $http.post('/dashboard/api/ApiDataManagePut', Indata).success(function (response) {
        });
    };
    return {
        put: vm.put
    }

}
DataManageServicePut.$inject = ['$http'];

angular.module('BlurAdmin.pages.charts.chartist').factory('DataManageServiceGet', DataManageServiceGet);
function DataManageServiceGet($resource, a) {
      return $resource('/api/ApiDataManagePut/:id', {
  //    return $resource('/dashboard/api/ApiDataManagePut/:id', {
        id: '@id',
    }, a);
}
DataManageServiceGet.$inject = ['$resource'];
