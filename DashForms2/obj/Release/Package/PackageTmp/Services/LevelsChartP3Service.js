﻿
angular.module('BlurAdmin.pages.charts.chartist').factory('LevelsChartP3Service', LevelsChartP3Service);
function LevelsChartP3Service($resource) {
    // return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/User_Form_Sum/:id', {
    return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/User_Form_Sum/:id', {
     //return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/User_Form_Sum/:id', { 
       id: '@id',
    });
}
LevelsChartP3Service.$inject = ['$resource'];
 
angular.module('BlurAdmin.pages.charts.chartist').factory('StatusChartP3Service', StatusChartP3Service);
function StatusChartP3Service($resource) {
     // return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/User_Form_Status/:id', {
      return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/User_Form_Status/:id', {
    //   return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/User_Form_Status/:id', {
        id: '@id',
    });
}
StatusChartP3Service.$inject = ['$resource'];
 
 
angular.module('BlurAdmin.pages.charts.chartist').factory('DashService', DashService);
function DashService($resource) {
    //return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/:id2/:id', {
        return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/:id2/:id', {
       //    return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/:id2/:id', {
        id: '@id',
        id2: '@id2',
    });
}
DashService.$inject = ['$resource'];

 
angular.module('BlurAdmin.pages.charts.chartist').factory('ReportService', ReportService);
function ReportService($resource, a) {
    //return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/Rpt_General_Form_Count/:id', {
    return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/Rpt_General_Form_Count', {
    //  return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/Rpt_General_Form_Count', {
        id: '@id',
        id2: '@id2',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
ReportService.$inject = ['$resource'];

angular.module('BlurAdmin.pages.charts.chartist').factory('DashboardPieService', DashboardPieService);
function DashboardPieService($resource, a) {
    //return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/St_Agaf_Form_Status', {
     return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/St_Agaf_Form_Status', {
        //  return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/St_Agaf_Form_Status', {
        id: '@id',
        id2: '@id2',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DashboardPieService.$inject = ['$resource'];


angular.module('BlurAdmin.pages.charts.chartist').factory('DashboardBarService', DashboardBarService);
function DashboardBarService($resource, a) {
    //return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/St_Form_State_Sla', {
        return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/St_Form_State_Sla', {
   //  return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/St_Form_State_Sla', {
        id: '@id',
        id2: '@id2',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DashboardBarService.$inject = ['$resource'];

angular.module('BlurAdmin.pages.charts.chartist').factory('DashboardDoughnutService', DashboardDoughnutService);
function DashboardDoughnutService($resource, a) {
    //return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/St_Top_After_Final_Minhal', {
       return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/St_Top_After_Final_Minhal', {
 // return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/St_Top_After_Final_Minhal', {
        id: '@id',
        id2: '@id2',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DashboardDoughnutService.$inject = ['$resource'];


angular.module('BlurAdmin.pages.charts.chartist').factory('DashboardLineService', DashboardLineService);
function DashboardLineService($resource, a) {
    //return $resource('https://jer-bina.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/St_Form_State_Sla_Trends', {
      return $resource('http://localhost:50920/api/v1.0/get_res_query_name/to_object/St_Form_State_Sla_Trends', {
    //    return $resource('http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/St_Form_State_Sla_Trends', {
        id: '@id',
        id2: '@id2',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DashboardLineService.$inject = ['$resource'];