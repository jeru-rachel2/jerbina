﻿angular.module('BlurAdmin.pages.charts.chartist').factory('LoginService', LoginService);
function LoginService($resource, a) {
   return $resource('/api/ApiLogin/:id', {
    //     return $resource('/dashboard/api/ApiLogin/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
LoginService.$inject = ['$resource'];