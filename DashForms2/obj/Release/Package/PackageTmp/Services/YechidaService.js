﻿angular.module('BlurAdmin.pages.charts.chartist').factory('YechidaService', YechidaService);
function YechidaService($resource, a) {
    return $resource('/api/ApiYechida/:id', {
      //   return $resource('/dashboard/api/ApiYechida/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
YechidaService.$inject = ['$resource'];