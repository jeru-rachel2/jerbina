﻿angular.module('BlurAdmin.pages.charts.chartist').factory('Report2Service', Report2Service);
function Report2Service($resource, a) {
        return $resource('/api/ApiReport2/:id', {
  //  return $resource('/dashboard/api/ApiReport2/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
Report2Service.$inject = ['$resource'];