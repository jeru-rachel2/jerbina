(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('reportCtrl', reportCtrl);
    function reportCtrl(OvedLogService, $location, $window, ReportService, ListService, $scope, Translate, $timeout, DateinYearService) {
        var stateRef = "report"
        $(document).on('click', ".Show", function (e) {
            e.stopImmediatePropagation();
            $scope.Show();
        });
        var WinNetWork = new ActiveXObject("WScript.Network");
        var temp = WinNetWork.UserName + "," + stateRef
        var arr = decodeURI($location.$$url.replace($location.$$path, '').replace('?', ''));
        var tempArr = arr.split(',');
        for (var i = 0; i < tempArr.length; i++) {
            tempArr[i] = tempArr[i].replace("%2F", "") + "01";
        }
        OvedLogService.post({ id: temp });
        DateinYearService.query(function (data) {
            $scope.Dates = data;
            var year = null;
            var MonthName = null;
            var j = -1;
            var k = 0;
            var l = 0;
            treedata_avm = [{ label: Translate('All'), children: [] }];
            for (var i = 0; i < data.length; i++) {
                if (data[i].Year != year) {
                    year = data[i].Year;
                    j++;
                    treedata_avm[0].children[j] = {};
                    treedata_avm[0].children[j].label = year;
                    treedata_avm[0].children[j].DateID = year;
                    if (tempArr.indexOf(year) > -1)
                        treedata_avm[0].children[j].checked = true;
                    else
                        treedata_avm[0].children[j].checked = false;
                    treedata_avm[0].children[j].children = [];
                    k = 0;
                }
                if (data[i].MonthName != MonthName) {
                    MonthName = data[i].MonthName;
                    k++;
                    treedata_avm[0].children[j].children[k] = {};
                    treedata_avm[0].children[j].children[k].DateID = data[i].DateID;
                    if (tempArr.indexOf(data[i].DateID) > -1)
                        treedata_avm[0].children[j].children[k].checked = true;
                    else
                        treedata_avm[0].children[j].children[k].checked = false;
                    treedata_avm[0].children[j].children[k].label = data[i].MonthName;
                    treedata_avm[0].children[j].children[k].label2 = data[i].enMon;
                }
            }
            $scope.my_data = treedata_avm;
            treedata_avm[0].checked = false;
            $scope.Show();
            var arr = decodeURI($location.$$url.replace($location.$$path, '').replace('?', '')).split(";");
            $scope.FormType = arr[0];
            var aa = arr[0].split(' ');
        })
        ListService.get(function (data) {
            $scope.StatusMeuchad = data.StatusMeuchad;
        });
        var tree, treedata_avm;
        $scope.divLoading = false;
        $scope.parking = false;
        $scope.mon = "";
        $scope.year = "";
        $scope.output = "";
        $scope.output2 = "";
        $scope.DateIDArr = [];
        $scope.ind = 0;
        $scope.my_tree_handler = function (branch) {
            var _ref;
            if (branch.level == 1) {
                for (var i = 0; i < treedata_avm[0].children.length; i++) {
                    if (treedata_avm[0].checked)
                        treedata_avm[0].children[i].checked = true;
                    else {
                        treedata_avm[0].children[i].checked = false;
                    }
                    for (var j = 0; j < treedata_avm[0].children[i].children.length; j++)
                        if (treedata_avm[0].checked)
                            treedata_avm[0].children[i].children[j].checked = true;
                        else
                            treedata_avm[0].children[i].children[j].checked = false;
                }
            }
            if (branch.DateID.length == 4) {
                for (var i = 0; i < treedata_avm[0].children.length; i++) {
                    if (branch.DateID == treedata_avm[0].children[i].DateID)
                        for (var j = 0; j < treedata_avm[0].children[i].children.length; j++) {
                            //if ($.inArray(treedata_avm[0].children[i].children[j].DateID, $scope.DateIDArr) == -1) {
                            if (treedata_avm[0].children[i].checked)
                                treedata_avm[0].children[i].children[j].checked = true;
                            else
                                treedata_avm[0].children[i].children[j].checked = false;
                        }
                }
            }
        };
        $scope.Show = function () {
            $scope.dat = "";
            if (treedata_avm[0].checked == true)
                $scope.dat = ".&AllMembers"
            else {
                for (var i = 0; i < treedata_avm[0].children.length; i++) {
                    for (var j = 0; j < treedata_avm[0].children[i].children.length; j++) {
                        if (treedata_avm[0].children[i].children[j] && treedata_avm[0].children[i].children[j].checked == true) {
                            if ($scope.dat != "")
                                $scope.dat += ",";
                            var year = treedata_avm[0].children[i].children[j].DateID.substring(0, 4);
                            var mon = treedata_avm[0].children[i].children[j].DateID.substring(4, 6);
                            if (mon.substring(0, 1) == "0")
                                mon = mon.substring(1, 2)
                            $scope.dat += ".&[" + year + "]" + "[" + mon + "]";
                        }
                    }
                }
            }
            var d = { 'id': $scope.dat };
            var jsonString = JSON.stringify(d);
            if ($scope.dat != "")
                $scope.divLoading = true;
            ReportService.post({ 'id': $scope.dat }, function (data) {
                $scope.divLoading = false;
                data.rowData[0][0] = Translate('Sum');
                $scope.tableData = data.rowData;
                for (var i = 0; i < $scope.tableData.length; i++) {
                    if ($scope.tableData[i][0].trim() == Translate('Park').trim()) {
                        $scope.parking = true;
                        $scope.tableData[i][9] = "red";
                    }
                }
                if (data.columnData) {
                    $scope.StatusMeuchadTh = [];
                    $scope.StatusMeuchadTh[0] = Translate(data.columnData[0]);
                    $scope.StatusMeuchadTh[1] = Translate(data.columnData[1]);
                    for (var i = 2; i < data.columnData.length; i++) {
                        var a = data.columnData[i].substring(1, data.columnData[i].length);
                        for (var j = 0; j < $scope.StatusMeuchad.length; j++) {
                            if ($scope.StatusMeuchad[j].Id == a) {
                                $scope.StatusMeuchadTh[i] = $scope.StatusMeuchad[j].Description
                                break;
                            }
                        }
                    }
                }
            }, function (data) {
            });
        }
        //begin excel
        var dataTable = [];
        var cols = [];
        $scope.ExcelExportData = function () {
            $scope.datesList = "";
            if ($scope.StatusMeuchadTh == null || $scope.StatusMeuchadTh[0] == null) {
                $window.alert(Translate('ExcelExportDataNull'));
                return;
            }
            cols = $scope.StatusMeuchadTh;
            var repTitlte = Translate('m2');
            var dates = $scope.dat.split('.');
            for (var i = 1; i < dates.length; i++) {
                var temp = dates[i].replace('&', '').replace('[', '').replace(']', '').replace(']', '').replace(',', '');
                var temp2 = temp.split('[');
                var date = temp2[1] + "/" + temp2[0];
                $scope.datesList += date + ',';
            }
            var table = '<table border="1" ><thead><tr><th colspan="9"><h3> ' + repTitlte + '</h3><h3>' + $scope.datesList + '</h3> </th></tr><tr>';
            for (var i = 0; i < cols.length; i++) {
                table += '<th style="background:blue;color:#fff">' + cols[i] + '</th>';
            }
            table += '</tr></thead><tbody>';
            for (var i = 0; i < $scope.tableData.length; i++) {
                table += '<tr>';
                var data = $scope.tableData[i];
                for (var j = 0; j < data.length; j++) {
                    if (data[j] != "")
                        table += '<td   >' + data[j] + '</td>';
                    else
                        table += '<td   ></td>';
                }
                table += '</tr>';
            }
            table += '</tbody></table>';
            var blob = new Blob([table], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Report.xls");
        };
        $scope.FormDrillClick = function (id) {
            $scope.datesList = "";
            var dates = $scope.dat.split('.');
            for (var i = 1; i < dates.length; i++) {
                var temp = dates[i].replace('&', '').replace('[', '').replace(']', '').replace(']', '').replace(',', '');
                var temp2 = temp.split('[');
                var date = temp2[0] + "/" + temp2[1];
                $scope.datesList += date + ',';
            }
            $location.url("/report2?" + id + ";" + $scope.datesList);
        }
        $scope.FormDrillClick2 = function (id2) {
            if (id2.$parent.$index != 0) {
                $scope.datesList = "";
                var dates = $scope.dat.split('.');
                for (var i = 1; i < dates.length; i++) {
                    var temp = dates[i].replace('&', '').replace('[', '').replace(']', '').replace(']', '').replace(',', '');
                    var temp2 = temp.split('[');
                    var date = temp2[0] + "/" + temp2[1];
                    $scope.datesList += date + ',';
                }
                var FormType = $scope.tableData[id2.$parent.$index][0];
                var Status = $scope.StatusMeuchadTh[id2.$index];
                $location.url("/report2?" + FormType + ";" + $scope.datesList + ";" + FormType + ";" + Status);
            }
        }
        $scope.my_tree = tree = {};
        return $scope.try_adding_a_branch = function () {
            var b;
            b = tree.get_selected_branch();
            return tree.add_branch(b, {
                label: 'New Branch',
                data: {
                    something: 42,
                    "else": 43
                }
            });
        };


       
    }
})();