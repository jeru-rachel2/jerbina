
(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('dataManageCtrl', dataManageCtrl);
    function dataManageCtrl(OvedLogService,$scope, Translate, DataManageService, DataManageServicePut, DataManageServiceGet, Notification) {
        var stateRef = "dataManage"
        var WinNetWork = new ActiveXObject("WScript.Network");
        var temp = WinNetWork.UserName + "," + stateRef
        OvedLogService.post({ id: temp });
        $scope.HolidayFlag = true;
        $scope.newHoliday = {};
        $scope.year = '2018';
        $('body').on('focusout', ".HebDate", function () {
            if($scope.MngHolidayList!=null)
            for (var i = 0; i < $scope.MngHolidayList.length; i++) {
                if (("s" == this.id.substring(0, 1)) && $scope.MngHolidayList[i].HolidayName === this.id.substring(1, $scope.MngHolidayList[i].HolidayName.length + 1)) {
                    $scope.MngHolidayList[i].HYStartDate = this.value;
                    break;
                }
                else if (("e" == this.id.substring(0, 1)) && $scope.MngHolidayList[i].HolidayName === this.id.substring(1, $scope.MngHolidayList[i].HolidayName.length + 1)) {
                    $scope.MngHolidayList[i].HYEndDate = this.value;
                    break;
                }
            }

        });
        $('body').on('focus', ".HebDate", function () {
            $(this).flexcal({
                position: 'rb',
                //changeMonth: true,
                changeYear: true,
                //current: new Date(),
                calendars: [['he-jewish', { dateFormat: 'dd/mm/yyyy' }], ['en', { dateFormat: 'dd/mm/yyyy' }]]
            })
        });

        DataManageServiceGet.query({ id: Translate($scope.year) }, function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].HYEndDate != null)
                    data[i].HYEndDate = data[i].HYEndDate.substring(0, 10);
                if (data[i].HYStartDate != null)
                    data[i].HYStartDate = data[i].HYStartDate.substring(0, 10);
            }
            $scope.MngHolidayList = data;

        });
        $scope.changeDate = function () {
            DataManageServiceGet.query({ id: Translate($scope.$$childTail.year) }, function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].HYEndDate != null)
                        data[i].HYEndDate = data[i].HYEndDate.substring(0, 10);
                    if (data[i].HYStartDate != null)
                        data[i].HYStartDate = data[i].HYStartDate.substring(0, 10);
                }
                $scope.MngHolidayList = data;

            });
        }
        $scope.AddDate = function () {
            var dStart = [];
            var dEnd = [];
            var HYStartDate = null;
            var HYEndDate = null;
            $scope.newHoliday.HYStartDate = $('#newStartDate')[0].value.substring(0, 10);;
            $scope.newHoliday.HYEndDate = $('#newEndDate')[0].value.substring(0, 10);;
            $scope.newHoliday.HYHebrewYear = Translate($scope.$$childTail.year);

            var dStart = [];
            var dEnd = [];
            var HYStartDate = null;
            var HYEndDate = null;
            if ($scope.newHoliday.HolidayName == null || $scope.newHoliday.HolidayName == "") {
                $scope.error(Translate("msg14"));
                return;
            }
            if ($scope.newHoliday.HolidayFactor == null || $scope.newHoliday.HolidayFactor == "") {
                $scope.error(Translate("msg15"));
                return;
            }
            if ($scope.newHoliday.HYStartDate != null && $scope.newHoliday.HYStartDate != "") {
                dStart = ($scope.newHoliday.HYStartDate).split('/');
                HYStartDate = new Date(dStart[2], dStart[1] - 1, dStart[0])
            }
            else {
                $scope.error(Translate("msg11"));
                return;
            }
            if ($scope.newHoliday.HYEndDate != null && $scope.newHoliday.HYEndDate != "") {
                dEnd = ($scope.newHoliday.HYEndDate).split('/');
                HYEndDate = new Date(dEnd[2], dEnd[1] - 1, dEnd[0])
            }
            else {
                $scope.error(Translate("msg11"));
                return;
            }
            if ((($scope.newHoliday.HYEndDate == null || $scope.newHoliday.HYEndDate == "") && ($scope.newHoliday.HYStartDate != null && $scope.newHoliday.HYStartDate != "")) || (($scope.newHoliday.HYStartDate == null || $scope.newHoliday.HYStartDate == "") && ($scope.newHoliday.HYEndDate != null && $scope.newHoliday.HYEndDate != ""))) {
                $scope.error(Translate("msg11"));
                return;
            }
            if (($scope.newHoliday.HYEndDate != null && $scope.newHoliday.HYEndDate != "") && ($scope.newHoliday.HYStartDate != null && $scope.newHoliday.HYStartDate != "")) {
                if (HYStartDate > HYEndDate) {
                    $scope.error(Translate("msg12"));
                    return;
                }
                if (HYStartDate.getFullYear() != $scope.$$childTail.year && HYStartDate.getFullYear() != ($scope.$$childTail.year - 1)) {
                    $scope.error(Translate("msg13"));
                    return;
                }
                if (HYEndDate.getFullYear() != $scope.$$childTail.year && HYEndDate.getFullYear() != (parseInt($scope.$$childTail.year) + 1) && HYEndDate.getFullYear() != (parseInt($scope.$$childTail.year) - 1)) {
                    $scope.error(Translate("msg13"));
                    return;
                }
            }
            $scope.newHoliday.HolidayID = parseInt(($scope.MngHolidayList[$scope.MngHolidayList.length - 1].HolidayID)) + 1;
            $scope.MngHolidayList.push($scope.newHoliday);
            DataManageService.post({ id: $scope.newHoliday }).$promise.then(function (data) {
                $scope.success(Translate("msg2"));
            }, function (error) {
                $scope.error(Translate("msg3"));
            });
            $scope.newHoliday = {};
            $('#newStartDate')[0].value = null;
            $('#newEndDate')[0].value = null;
        }
        $scope.HolidayIDChecked = function (id) {
            if (id.HolidayFlag == false)
            {
                $scope.HolidayID2 = id.HolidayID;
                $scope.HolidayFlag = true;
            }
            else
                $scope.HolidayFlag = false;
        }
        $scope.DeleteDate = function () {
            if( $scope.HolidayFlag == false)
                $scope.error(Translate("msg21"));
          else  if ($scope.HolidayID2 == null || $scope.HolidayID2 == "")
                $scope.error(Translate("msg16"));
            else
                $scope.successHtml2();
        }

        $scope.SaveDates = function () {
            for (var i = 0; i < $scope.MngHolidayList.length; i++) {
                var dStart = [];
                var dEnd = [];
                var HYStartDate = null;
                var HYEndDate = null;
                if ($scope.MngHolidayList[i].HYStartDate != null && $scope.MngHolidayList[i].HYStartDate != "") {
                    dStart = ($scope.MngHolidayList[i].HYStartDate).split('/');
                    HYStartDate = new Date(dStart[2], dStart[1] - 1, dStart[0])
                }
                if ($scope.MngHolidayList[i].HYEndDate != null && $scope.MngHolidayList[i].HYEndDate != "") {
                    dEnd = ($scope.MngHolidayList[i].HYEndDate).split('/');
                    HYEndDate = new Date(dEnd[2], dEnd[1] - 1, dEnd[0])
                }
                if ((($scope.MngHolidayList[i].HYEndDate == null || $scope.MngHolidayList[i].HYEndDate == "") && ($scope.MngHolidayList[i].HYStartDate != null && $scope.MngHolidayList[i].HYStartDate != "")) || (($scope.MngHolidayList[i].HYStartDate == null || $scope.MngHolidayList[i].HYStartDate == "") && ($scope.MngHolidayList[i].HYEndDate != null && $scope.MngHolidayList[i].HYEndDate != ""))) {
                    $scope.error(Translate("msg11"));
                    return;
                }
                if (($scope.MngHolidayList[i].HYEndDate != null && $scope.MngHolidayList[i].HYEndDate != "") && ($scope.MngHolidayList[i].HYStartDate != null && $scope.MngHolidayList[i].HYStartDate != "")) {
                    //if (HYStartDate > HYEndDate) {
                    //    $scope.error(Translate("msg12"));
                    //    return;
                    //}
                    if (parseInt(dStart[2]) != $scope.$$childTail.year && parseInt(dStart[2]) != $scope.$$childTail.year - 1) {
                        $scope.error(Translate("msg13"));
                        return;
                    }
                    if (parseInt(dEnd[2]) != $scope.$$childTail.year && parseInt(dEnd[2]) != (parseInt($scope.$$childTail.year) + 1) && parseInt(dEnd[2]) != (parseInt($scope.$$childTail.year) - 1)) {
                          $scope.error(Translate("msg13"));
                        return;
                    }
                }
            }
            DataManageServicePut.put(Translate($scope.$$childTail.year), $scope.MngHolidayList).then(function (data) {
                $scope.success(Translate("msg2"));
            }, function (error) {
                $scope.error(Translate("msg3"));
            });

        }
        $scope.error = function (msg) {
            Notification.error({ message: msg, delay: 1500 });
        };
        $scope.success = function (msg) {
            Notification.success({ message: msg, delay: 1500 });
        };
        $scope.warning = function (msg) {
            Notification.warning({ message: msg, delay: 1500 });
        };
        $scope.removeYes2 = function () {
            DataManageService.get({ id: $scope.HolidayID2 }).$promise.then(function (data) {
                $scope.success(Translate("msg2"));
                for (var j = 0; j < $scope.MngHolidayList.length; j++) {
                    if ($scope.MngHolidayList[j].HolidayID === $scope.HolidayID2.toString()) {
                        $("#s" + $scope.MngHolidayList[j].HolidayName)[0].value = null;
                        $("#e" + $scope.MngHolidayList[j].HolidayName)[0].value = null;
                        break;
                    }
                }
            }, function (error) {
                $scope.error(Translate("msg3"));
            });

        }
        $scope.successHtml2 = function () {
            Notification.success({ message: '<div><label>' + Translate("msg5") + '</label></br><button id="btnPrepend2"  class="form-control" style="display: inline; max-width: 60px; margin-top: 10px;   max-height: 25px;   padding: 5px;text-align: center; " >' + Translate("msg9") + '</button><button  class="form-control" style=" margin-left: 45px;display: inline;max-width: 60px; margin-top: 10px;   max-height: 25px;   padding: 5px;text-align: center; " >' + Translate("msg10") + '</button></div>', delay: 150000 });
        };
        $(document).on('click', '#btnPrepend2', function () {
            $scope.removeYes2();
        });

    }
})();