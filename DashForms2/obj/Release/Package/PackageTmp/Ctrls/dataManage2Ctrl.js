(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('dataManage2Ctrl', dataManage2Ctrl);
    function dataManage2Ctrl(OvedLogService,$scope, Translate, DataManage2Service, Notification) {
        var stateRef = "dataManage2"
        var WinNetWork = new ActiveXObject("WScript.Network");
        var temp = WinNetWork.UserName + "," + stateRef
        OvedLogService.post({ id: temp });
        $scope.formDetails = {};
        $scope.formDetailsFlag = false;
        $scope.getForm = function () {
            if (isNaN(parseInt($scope.$$childTail.FormId))) {
                $scope.error(Translate("msg6"));
                $scope.$$childTail.FormId = null;
            }
            else {
                DataManage2Service.get({ id: $scope.$$childTail.FormId }, function (data) {
                    $scope.formDetails = data;
                    if (data.FormName != null)
                        $scope.formDetailsFlag = true;
                    else {
                        $scope.formDetailsFlag = false;
                        $scope.error(Translate("msg4"));
                    }
                })
            }
        }
        $scope.save = function () {
            if ($scope.$$childTail.FormId == null || $scope.$$childTail.FormId == "")
                $scope.error(Translate("msg7"));
            else if ($scope.formDetails.DiscardSlaDesc == null || $scope.formDetails.DiscardSlaDesc == "")
                $scope.error(Translate("msg20"));
            else if (isNaN(parseInt($scope.$$childTail.FormId))) {
                $scope.error(Translate("msg6"));
                $scope.$$childTail.FormId = null;
            }
            else if ($scope.formDetails.FormName == null)
                $scope.error(Translate("msg4"));
            else {
                $scope.formDetails.DiscardSlaObjectID = $scope.$$childTail.FormId,
                $scope.formDetails.DiscardSlaObjectTypeID = 1,
                $scope.formDetails.DiscardSlaUserID = "12163630"
                DataManage2Service.post({ id: $scope.formDetails }).$promise.then(function (data) {
                    $scope.success(Translate("msg2"));
                }, function (error) {
                    $scope.error(Translate("msg3"));
                });
                //$scope.formDetailsFlag = false;
                //$scope.formDetails = {};
                //$scope.$$childTail.FormId = ""
            }
        }
        $scope.cancel = function () {
            $scope.formDetails = {};
            $scope.formDetailsFlag = false;
            $scope.$$childTail.FormId = ""
        }
        $scope.remove = function () {
            if ($scope.$$childTail.FormId == null || $scope.$$childTail.FormId == "")
                $scope.error(Translate("msg7"));
            else if (isNaN(parseInt($scope.$$childTail.FormId))) {
                $scope.error(Translate("msg6"));
                $scope.$$childTail.FormId = null;
            }
            else if ($scope.formDetails.FormName == null)
                $scope.error(Translate("msg4"));
            else {
                $scope.successHtml();
            }
        }
        $scope.removeYes = function () {
            DataManage2Service.get({ id: $scope.formDetails.FormReference, type: 1 }).$promise.then(function (data) {
                $scope.success(Translate("msg2"));
                $scope.formDetailsFlag = false;
                $scope.formDetails = {};
                if ($scope.$$childTail)
                    $scope.$$childTail.FormId = ""
            }, function (error) {
                $scope.error(Translate("msg3"));
            });

        }
        $scope.error = function (msg) {
            Notification.error({ message: msg, delay: 1500 });
        };
        $scope.success = function (msg) {
            Notification.success({ message: msg, delay: 1500 });
        };
        $scope.warning = function (msg) {
            Notification.warning({ message: msg, delay: 1500 });
        };
        $scope.successHtml = function () {
            Notification.success({ message: '<div><label>' + Translate("msg5") + '</label></br><button id="btnPrepend8"  class="form-control" style="display: inline; max-width: 60px; margin-top: 10px;   max-height: 25px;   padding: 5px;text-align: center; " >' + Translate("msg9") + '</button><button  class="form-control" style=" margin-left: 45px;display: inline;max-width: 60px; margin-top: 10px;   max-height: 25px;   padding: 5px;text-align: center; " >' + Translate("msg10") + '</button></div>', delay: 150000 });
        };
        $(document).on('click', '#btnPrepend8', function () {
            $scope.removeYes();
        });
    }
})();