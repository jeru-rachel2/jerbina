(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('StartCtrl', StartCtrl);
    function StartCtrl(StartService, $scope, Translate, $window, $timeout) {
        StartService.get(function (data) {
            if (data != null && data.toString() && data[0] != null && data[0].toString() != null) {
                $("#preLoad").hide();
                $("body").removeClass("overlay");
            }
            else { 
                function incLoader() {
                    var t = 0;
                    var x = setInterval(function () {
                        t++;
                        $('.loader span').text(t + '%');
                        if (t == 100) {
                            clearInterval(x);
                            if (window.location.hash.indexOf("loaded") == -1) {
                                window.location = window.location + '#loaded';
                                window.location.reload();
                            }
                        };
                    }, 1000);
                };
                if (window.location.hash.indexOf("loaded") == -1) {
                    incLoader()
                }
                else {
                    $("#preLoad").hide();
                    $("body").removeClass("overlay");
                }

            }
        })
    }
})();