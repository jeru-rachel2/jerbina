﻿using DashForms2.Controllers.Api;
using DashForms2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
namespace DashForms2
{

    public class Connect_DB
    {
        public DataSet ExecProcedure(string sProcedureName, List<SqlParameter> lParams)
        {
            try
            {
                DataSet ds = new DataSet();
                string connectionString = ConfigurationManager.ConnectionStrings["TFASIM_SLA"].ToString();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = new SqlCommand(sProcedureName, conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        if (lParams != null)
                            foreach (var item in lParams)
                            {
                                da.SelectCommand.Parameters.Add(item);
                            }
                        da.Fill(ds);
                    }
                }
                return ds;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        internal List<MyYechida> GetYechidot()
        {
            List<MyYechida> YechidasList = new List<MyYechida>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            try
            {

                var item1 = ExecProcedure("SP_MNG_SP_MNG_GetYechidot", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    MyYechida myYechida = new MyYechida();
                    myYechida.MINHALA = item["MINHALA"] != DBNull.Value ? item["MINHALA"].ToString() : null;
                    myYechida.TEUR_MINHALA = item["TEUR_MINHALA"] != DBNull.Value ? item["TEUR_MINHALA"].ToString() : null;
                    myYechida.AGAF = item["AGAF"] != DBNull.Value ? item["AGAF"].ToString() : null;
                    myYechida.TEUR_AGAF = item["TEUR_AGAF"] != DBNull.Value ? item["TEUR_AGAF"].ToString() : null;
                    myYechida.RequestTypeID = item["RequestTypeID"] != DBNull.Value ? item["RequestTypeID"].ToString() : null;
                    myYechida.RequestTypeName = item["RequestTypeName"] != DBNull.Value ? item["RequestTypeName"].ToString() : null;
                    //myYechida.MAHLAKA = item["MAHLAKA"] != DBNull.Value ? item["MAHLAKA"].ToString() : null;
                    //myYechida.TEUR_MAHLAKA = item["TEUR_MAHLAKA"] != DBNull.Value ? item["TEUR_MAHLAKA"].ToString() : null;
                    //myYechida.YECHIDA = item["YECHIDA"] != DBNull.Value ? item["YECHIDA"].ToString() : null;
                    //myYechida.TEUR_YECHIDA = item["TEUR_YECHIDA"] != DBNull.Value ? item["TEUR_YECHIDA"].ToString() : null;
                    YechidasList.Add(myYechida);
                }
                return YechidasList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public string GetSumForms()
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            string sum = null;
            try
            {
                var item = ExecProcedure("SP_MNG_GetSumRowsFactSlaBase", lParams).Tables[0].Rows[0];
                sum = item["sum"] != DBNull.Value ? item["sum"].ToString() : null;
                return sum;
            }
            catch (Exception ex)
            {
                sum = ex.Message;
            }
            return sum;
        }

        internal List<Enteries> GeneralReportGetAll(string id)
        {
            List<Enteries> enteries = new List<Enteries>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            if (id != null && id != "")
            {
                var arr = id.Split(',');
                if (arr[0] != "null" && arr[0] != null && arr[0] != "")
                {
                    arr[0] = arr[0].Replace('-', '/');
                    arr[0] = arr[0].Replace('-', '/');
                    var arr2 = arr[0].Split('/');
                    string str = arr2[1] + '/' + arr2[0] + '/' + arr2[2];
                    lParams.Add(new SqlParameter("@fromDate", str));
                }
                if (arr[1] != "null" && arr[1] != null && arr[1] != "")
                {
                    arr[1] = arr[1].Replace('-', '/');
                    arr[1] = arr[1].Replace('-', '/');
                    var arr2 = arr[1].Split('/');
                    string str = arr2[1] + '/' + arr2[0] + '/' + arr2[2];
                    lParams.Add(new SqlParameter("@toDate", str));
                }
            }
            try
            {
                var item1 = ExecProcedure("SP_Mng_DiscardSlaGetAll", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    Enteries entery = new Enteries();
                    //entery.userName = item["userName"] != DBNull.Value ? item["userName"].ToString() : null;
                    entery.Agaf = item["Agaf"] != DBNull.Value ? item["Agaf"].ToString() : null;
                    //entery.IdAgaf = item["IdAgaf"] != DBNull.Value ? item["IdAgaf"].ToString() : null;
                    entery.AgafCnt = item["AgafCnt"] != DBNull.Value ? item["AgafCnt"].ToString() : null;
                    enteries.Add(entery);
                }
                return enteries;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Enteries> Fact_OvedLogGetAll(string id)
        {
            List<Enteries> enteries = new List<Enteries>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            if (id != null && id != "")
            {
                var arr = id.Split(',');
                if (arr[0] != "null" && arr[0] != null && arr[0] != "")
                {
                    arr[0] = arr[0].Replace('-', '/');
                    arr[0] = arr[0].Replace('-', '/');
                    var arr2 = arr[0].Split('/');
                    string str = arr2[1] + '/' + arr2[0] + '/' + arr2[2];
                    lParams.Add(new SqlParameter("@fromDate", str));
                }
                if (arr[1] != "null" && arr[1] != null && arr[1] != "")
                {
                    arr[1] = arr[1].Replace('-', '/');
                    arr[1] = arr[1].Replace('-', '/');
                    var arr2 = arr[1].Split('/');
                    string str = arr2[1] + '/' + arr2[0] + '/' + arr2[2];
                    lParams.Add(new SqlParameter("@toDate", str));
                }
            }
            try
            {
                var item1 = ExecProcedure("SP_MNG_Fact_OvedLogGetAll", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    Enteries entery = new Enteries();
                    //entery.userName = item["userName"] != DBNull.Value ? item["userName"].ToString() : null;
                    entery.Agaf = item["Agaf"] != DBNull.Value ? item["Agaf"].ToString() : null;
                    //entery.IdAgaf = item["IdAgaf"] != DBNull.Value ? item["IdAgaf"].ToString() : null;
                    entery.AgafCnt = item["AgafCnt"] != DBNull.Value ? item["AgafCnt"].ToString() : null;
                    enteries.Add(entery);
                }
                return enteries;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal void Fact_OvedLogInsert(string userName, string screen)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@OvedAD", userName));
            lParams.Add(new SqlParameter("@ScreenID", screen));
            try
            {
                ExecProcedure("SP_MNG_Fact_OvedLogInsert", lParams);
            }
            catch (Exception ex)
            {
            }
        }
        internal List<FactSlaShort> GetReport2(string id, string id2, string id3, string id4, string id5)
        {
            List<FactSlaShort> factSlaList = new List<FactSlaShort>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            if ((id5 == "שם טופס" || id5 == "הכל") && (id2 == "שם טופס" || id2 == "הכל"))
                id2 = null;
            if (id2 == "כל הסטטוסים")
                id2 = id5;

            lParams.Add(new SqlParameter("@id2", id2));
            //lParams.Add(new SqlParameter("@id3", Convert.ToDateTime(id3).Month));
            //lParams.Add(new SqlParameter("@id4", Convert.ToDateTime(id3).Year)) 
            lParams.Add(new SqlParameter("@id3", id3.Substring(0, id3.Length - 1)));
            try
            {
                var item1 = new DataTable();
                if (id4 == "1")
                    item1 = ExecProcedure("SP_MNG_GetAllFactSlaBaseToTypeAnsStatus", lParams).Tables[0];
                else
                    item1 = ExecProcedure("SP_MNG_GetAllFactSlaBaseToTypeAnsStatusAll", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    FactSlaShort factSla = new FactSlaShort();
                    factSla.Id = item["FormReference"] != DBNull.Value ? item["FormReference"].ToString() : null;
                    factSla.TypeFormName = item["FormName"] != DBNull.Value ? item["FormName"].ToString() : null;
                    factSla.CreateDate = item["CreateDate"] != DBNull.Value ? item["CreateDate"].ToString() : null;
                    factSla.UpdateDate = item["UpdateDate"] != DBNull.Value ? item["UpdateDate"].ToString() : null;
                    factSla.SenderFirstName = item["SenderFirstName"] != DBNull.Value ? item["SenderFirstName"].ToString() : null;
                    factSla.SenderLastName = item["SenderLastName"] != DBNull.Value ? item["SenderLastName"].ToString() : null;
                    factSla.StatusName = item["UnifiedStageName"] != DBNull.Value ? item["UnifiedStageName"].ToString() : null;
                    factSla.HeaderText = item["HeaderText"] != DBNull.Value ? item["HeaderText"].ToString() : null;
                    factSlaList.Add(factSla);
                }
                return factSlaList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        internal List<MyDate> GetDates()
        {
            List<MyDate> DatesList = new List<MyDate>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            try
            {

                var item1 = ExecProcedure("SP_MNG_GetDates", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    MyDate sideBarMenu = new MyDate();
                    sideBarMenu.Year = item["Year"] != DBNull.Value ? item["Year"].ToString() : null;
                    sideBarMenu.MonthName = item["MonthName"] != DBNull.Value ? item["MonthName"].ToString() : null;
                    sideBarMenu.DayOfMonth = item["DayOfMonth"] != DBNull.Value ? item["DayOfMonth"].ToString() : null;
                    sideBarMenu.enMon = item["enMon"] != DBNull.Value ? item["enMon"].ToString() : null;
                    sideBarMenu.DateID = item["DateID"] != DBNull.Value ? item["DateID"].ToString() : null;
                    DatesList.Add(sideBarMenu);
                }
                return DatesList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<GuiScreen> GuiScreenElementQuery(string id)
        {
            List<GuiScreen> GuiScreenList = new List<GuiScreen>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            try
            {

                var item1 = ExecProcedure("SP_MNG_GuiScreenElementQuery", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    GuiScreen sideBarMenu = new GuiScreen();
                    sideBarMenu.ElementTypeName = item["ElementTypeName"] != DBNull.Value ? item["ElementTypeName"].ToString() : null;
                    sideBarMenu.BiQueryDesc = item["BiQueryDesc"] != DBNull.Value ? item["BiQueryDesc"].ToString() : null;
                    sideBarMenu.BiQueryToolTip = item["BiQueryToolTip"] != DBNull.Value ? item["BiQueryToolTip"].ToString() : null;
                    sideBarMenu.BiQueryName = item["BiQueryName"] != DBNull.Value ? item["BiQueryName"].ToString() : null;
                    sideBarMenu.BiQueryLink = item["BiQueryLink"] != DBNull.Value ? item["BiQueryLink"].ToString() : null;
                    sideBarMenu.BiQueryResultFormat = item["BiQueryResultFormat"] != DBNull.Value ? item["BiQueryResultFormat"].ToString() : null;
                    GuiScreenList.Add(sideBarMenu);
                }
                return GuiScreenList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public List<Mng_SideBarMenu> GetUsersGroupsByIdm(string id)
        {
            List<Mng_SideBarMenu> UsersideBarMenu = new List<Mng_SideBarMenu>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@IdmGroups", id));
            try
            {

                var item1 = ExecProcedure("SP_ManagePermission", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    Mng_SideBarMenu sideBarMenu = new Mng_SideBarMenu();
                    sideBarMenu.Id = item["Id"] != DBNull.Value ? int.Parse(item["Id"].ToString()) : default(int);
                    sideBarMenu.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                    sideBarMenu.stateRef = item["stateRef"] != DBNull.Value ? item["stateRef"].ToString() : null;
                    sideBarMenu.IsFather = item["IsFather"] != DBNull.Value ? bool.Parse(item["IsFather"].ToString()) : default(bool);
                    sideBarMenu.FatherGroups = item["FatherGroups"] != DBNull.Value ? int.Parse(item["FatherGroups"].ToString()) : default(int);
                    sideBarMenu.G4200201 = item["G4200201"] != DBNull.Value ? bool.Parse(item["G4200201"].ToString()) : default(bool);
                    sideBarMenu.G4200202 = item["G4200202"] != DBNull.Value ? bool.Parse(item["G4200202"].ToString()) : default(bool);
                    sideBarMenu.G4200203 = item["G4200203"] != DBNull.Value ? bool.Parse(item["G4200203"].ToString()) : default(bool);
                    sideBarMenu.G4200301 = item["G4200301"] != DBNull.Value ? bool.Parse(item["G4200301"].ToString()) : default(bool);
                    sideBarMenu.G4200302 = item["G4200302"] != DBNull.Value ? bool.Parse(item["G4200302"].ToString()) : default(bool);
                    sideBarMenu.G42004 = item["G42004"] != DBNull.Value ? bool.Parse(item["G42004"].ToString()) : default(bool);
                    UsersideBarMenu.Add(sideBarMenu);
                }
                return UsersideBarMenu;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<FactSla> GetAllDashboard3ToType(string id, string id2)
        {
            List<FactSla> factSlaList = new List<FactSla>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            lParams.Add(new SqlParameter("@id2", id2));
            try
            {

                var item1 = ExecProcedure("SP_MNG_GetAllFactSlaBaseToType", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    FactSla factSla = new FactSla();
                    factSla.Id = item["FormReference"] != DBNull.Value ? item["FormReference"].ToString() : null;
                    factSla.TypeFormId = item["FormTypeID"] != DBNull.Value ? item["FormTypeID"].ToString() : null;
                    factSla.TypeFormName = item["FormName"] != DBNull.Value ? item["FormName"].ToString() : null;
                    factSla.CreateDate = item["CreateDate"] != DBNull.Value ? item["CreateDate"].ToString() : null;
                    factSla.StatusId = item["UnifiedStageID"] != DBNull.Value ? item["UnifiedStageID"].ToString() : null;
                    factSla.UpdateDate = item["UpdateDate"] != DBNull.Value ? item["UpdateDate"].ToString() : null;
                    factSla.SenderFirstName = item["SenderFirstName"] != DBNull.Value ? item["SenderFirstName"].ToString() : null;
                    factSla.SenderLastName = item["SenderLastName"] != DBNull.Value ? item["SenderLastName"].ToString() : null;
                    factSla.StatusName = item["UnifiedStageName"] != DBNull.Value ? item["UnifiedStageName"].ToString() : null;
                    factSla.Sla = item["NetJerSlaDaysLeftStage"] != DBNull.Value ? item["NetJerSlaDaysLeftStage"].ToString() : null;
                    factSla.SlaColor = item["NetJerSlaDaysHist"] != DBNull.Value ? item["NetJerSlaDaysHist"].ToString() : null;
                    factSla.IsStatusRed = item["IsStatusRed"] != DBNull.Value ? bool.Parse(item["IsStatusRed"].ToString()) : false;
                    factSla.IsStatusGreen = item["IsStatusGreen"] != DBNull.Value ? bool.Parse(item["IsStatusGreen"].ToString()) : false;
                    factSla.IsStatusYellow = item["IsStatusYellow"] != DBNull.Value ? bool.Parse(item["IsStatusYellow"].ToString()) : false;
                    factSla.IsHistStatusRed = item["IsHistStatusRed"] != DBNull.Value ? bool.Parse(item["IsHistStatusRed"].ToString()) : false;
                    factSla.IsHistStatusGreen = item["IsHistStatusGreen"] != DBNull.Value ? bool.Parse(item["IsHistStatusGreen"].ToString()) : false;
                    factSla.IsHistStatusYellow = item["IsHistStatusYellow"] != DBNull.Value ? bool.Parse(item["IsHistStatusYellow"].ToString()) : false;
                    factSla.HeaderText = item["HeaderText"] != DBNull.Value ? item["HeaderText"].ToString() : null;
                    factSla.SumIsReturned = item["SumIsReturned"] != DBNull.Value ? item["SumIsReturned"].ToString() : null;
                    factSlaList.Add(factSla);
                }
                return factSlaList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<FactSla> GetAllDashboard3ToStatus(string id, string id2)
        {
            List<FactSla> factSlaList = new List<FactSla>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            id2 = id2.Substring(0, id2.Length - 2);
            lParams.Add(new SqlParameter("@id2", id2));
            try
            {

                var item1 = ExecProcedure("SP_MNG_GetAllFactSlaBaseToStatus", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    FactSla factSla = new FactSla();
                    factSla.Id = item["FormReference"] != DBNull.Value ? item["FormReference"].ToString() : null;
                    factSla.TypeFormId = item["FormTypeID"] != DBNull.Value ? item["FormTypeID"].ToString() : null;
                    factSla.TypeFormName = item["FormName"] != DBNull.Value ? item["FormName"].ToString() : null;
                    factSla.CreateDate = item["CreateDate"] != DBNull.Value ? item["CreateDate"].ToString() : null;
                    factSla.StatusId = item["UnifiedStageID"] != DBNull.Value ? item["UnifiedStageID"].ToString() : null;
                    factSla.StatusName = item["UnifiedStageName"] != DBNull.Value ? item["UnifiedStageName"].ToString() : null;
                    factSla.UpdateDate = item["UpdateDate"] != DBNull.Value ? item["UpdateDate"].ToString() : null;
                    factSla.SenderFirstName = item["SenderFirstName"] != DBNull.Value ? item["SenderFirstName"].ToString() : null;
                    factSla.SenderLastName = item["SenderLastName"] != DBNull.Value ? item["SenderLastName"].ToString() : null;
                    factSla.Sla = item["NetJerSlaDaysLeftStage"] != DBNull.Value ? item["NetJerSlaDaysLeftStage"].ToString() : null;
                    factSla.SlaColor = item["NetJerSlaDaysHist"] != DBNull.Value ? item["NetJerSlaDaysHist"].ToString() : null;
                    factSla.IsStatusRed = item["IsStatusRed"] != DBNull.Value ? bool.Parse(item["IsStatusRed"].ToString()) : false;
                    factSla.IsStatusGreen = item["IsStatusGreen"] != DBNull.Value ? bool.Parse(item["IsStatusGreen"].ToString()) : false;
                    factSla.IsStatusYellow = item["IsStatusYellow"] != DBNull.Value ? bool.Parse(item["IsStatusYellow"].ToString()) : false;
                    factSla.IsHistStatusRed = item["IsHistStatusRed"] != DBNull.Value ? bool.Parse(item["IsHistStatusRed"].ToString()) : false;
                    factSla.IsHistStatusGreen = item["IsHistStatusGreen"] != DBNull.Value ? bool.Parse(item["IsHistStatusGreen"].ToString()) : false;
                    factSla.IsHistStatusYellow = item["IsHistStatusYellow"] != DBNull.Value ? bool.Parse(item["IsHistStatusYellow"].ToString()) : false;
                    factSla.HeaderText = item["HeaderText"] != DBNull.Value ? item["HeaderText"].ToString() : null;
                    factSla.SumIsReturned = item["SumIsReturned"] != DBNull.Value ? item["SumIsReturned"].ToString() : null;
                    factSlaList.Add(factSla);
                }
                return factSlaList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public Myclass GetDate()
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            try
            {

                var item1 = ExecProcedure("SP_MNG_GetMng_BiUpdateDateTime", lParams).Tables[0];
                Myclass str2 = new Myclass();
                str2.str = item1.Rows[0].ItemArray[0].ToString();
                return str2;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<FactSla> GetAllDashboard3(string id)
        {
            List<FactSla> factSlaList = new List<FactSla>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            try
            {

                var item1 = ExecProcedure("SP_MNG_GetAllFactSlaBase", lParams).Tables[0];
                for (int i = 0; i < item1.Rows.Count; i++)
                {
                    DataRow item = item1.Rows[i];
                    FactSla factSla = new FactSla();
                    factSla.Id = item["FormReference"] != DBNull.Value ? item["FormReference"].ToString() : null;
                    factSla.TypeFormId = item["FormTypeID"] != DBNull.Value ? item["FormTypeID"].ToString() : null;
                    factSla.TypeFormName = item["FormName"] != DBNull.Value ? item["FormName"].ToString() : null;
                    factSla.CreateDate = item["CreateDate"] != DBNull.Value ? item["CreateDate"].ToString() : null;
                    factSla.StatusId = item["UnifiedStageID"] != DBNull.Value ? item["UnifiedStageID"].ToString() : null;
                    factSla.UpdateDate = item["UpdateDate"] != DBNull.Value ? item["UpdateDate"].ToString() : null;
                    factSla.SenderFirstName = item["SenderFirstName"] != DBNull.Value ? item["SenderFirstName"].ToString() : null;
                    factSla.SenderLastName = item["SenderLastName"] != DBNull.Value ? item["SenderLastName"].ToString() : null;
                    factSla.StatusName = item["UnifiedStageName"] != DBNull.Value ? item["UnifiedStageName"].ToString() : null;
                    factSla.Sla = item["NetJerSlaDaysLeftStage"] != DBNull.Value ? item["NetJerSlaDaysLeftStage"].ToString() : null;
                    factSla.SlaColor = item["NetJerSlaDaysHist"] != DBNull.Value ? item["NetJerSlaDaysHist"].ToString() : null;
                    factSla.IsStatusRed = item["IsStatusRed"] != DBNull.Value ? bool.Parse(item["IsStatusRed"].ToString()) : false;
                    factSla.IsStatusGreen = item["IsStatusGreen"] != DBNull.Value ? bool.Parse(item["IsStatusGreen"].ToString()) : false;
                    factSla.IsStatusYellow = item["IsStatusYellow"] != DBNull.Value ? bool.Parse(item["IsStatusYellow"].ToString()) : false;
                    factSla.IsHistStatusRed = item["IsHistStatusRed"] != DBNull.Value ? bool.Parse(item["IsHistStatusRed"].ToString()) : false;
                    factSla.IsHistStatusGreen = item["IsHistStatusGreen"] != DBNull.Value ? bool.Parse(item["IsHistStatusGreen"].ToString()) : false;
                    factSla.IsHistStatusYellow = item["IsHistStatusYellow"] != DBNull.Value ? bool.Parse(item["IsHistStatusYellow"].ToString()) : false;
                    factSla.HeaderText = item["HeaderText"] != DBNull.Value ? item["HeaderText"].ToString() : null;
                    factSla.SumIsReturned = item["SumIsReturned"] != DBNull.Value ? item["SumIsReturned"].ToString() : null;
                    if (factSla.SumIsReturned != "0" && factSla.SumIsReturned != null)
                        factSla.StatusName += " ( " + factSla.SumIsReturned + " )";
                    factSlaList.Add(factSla);
                }
                return factSlaList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public void DeleteMngStage2InRequest(string id)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@RequestTypeID", id));
            ListModel list = new ListModel();
            try
            {
                ExecProcedure("SP_MNG_DeleteMngStage2InRequest", lParams);
            }
            catch (Exception ex)
            {
            }
        }

        public void DeleteMngStageInRequest(string id, string id2)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@UnifiedStageID", id));
            lParams.Add(new SqlParameter("@RequestTypeID", id2));
            ListModel list = new ListModel();
            try
            {
                ExecProcedure("SP_MNG_DeleteMngStageInRequest", lParams);
            }
            catch (Exception ex)
            {
            }
        }

        public void DeleteMngStageInRequest(SlaMng mh2)
        {
            for (int i = 0; i < mh2.FormStageID.Count(); i++)
            {
                List<SqlParameter> lParams = new List<SqlParameter>();
                lParams.Add(new SqlParameter("@UnifiedStageID", mh2.UnifiedStageID));
                lParams.Add(new SqlParameter("@FormStageID", mh2.FormStageID[i].Id));
                lParams.Add(new SqlParameter("@RequestTypeID", mh2.RequestTypeID));
                ListModel list = new ListModel();
                try
                {
                    ExecProcedure("SP_MNG_DeleteMngStageInRequest", lParams);
                }
                catch (Exception ex)
                {

                }
            }
        }

        public SlaMngHis GetMngStage2InRequestById(string id)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            try
            {
                DataRow item = null;
                var item1 = ExecProcedure("SP_MNG_GetMngStage2InRequestById", lParams).Tables[0];
                if (item1 != null && item1.Rows.Count > 0)
                {
                    item = item1.Rows[0];
                    SlaMngHis slaMng = new SlaMngHis();
                    slaMng.RequestTypeID = item["RequestTypeID"] != DBNull.Value ? item["RequestTypeID"].ToString() : null;
                    slaMng.SlaHistGreen = item["RHIGreen"] != DBNull.Value ? item["RHIGreen"].ToString() : null;
                    slaMng.SlaHistYellow = item["RHIYellow"] != DBNull.Value ? item["RHIYellow"].ToString() : null;
                    slaMng.SlaHistRed = item["RHIRed"] != DBNull.Value ? item["RHIRed"].ToString() : null;
                    slaMng.SlaPeriodTypeID = item["SlaPeriodTypeID"] != DBNull.Value ? item["SlaPeriodTypeID"].ToString() : null;
                    return slaMng;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<SlaMng> GetMngStageInRequestById(string id)
        {
            var arr = id.Split(',');
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", arr[0]));
            if (arr.Count() > 1)
                lParams.Add(new SqlParameter("@UnifiedStageID", arr[1]));
            List<SlaMng> slaMngList = new List<SlaMng>();
            try
            {
                var a = ExecProcedure("SP_MNG_GetMngStageInRequestById", lParams);
                SlaMng slaMng = new SlaMng();
                if (a.Tables[0].Rows.Count > 0)
                {
                    DataRow item = a.Tables[0].Rows[0];
                    slaMng.UnifiedStageID = item["UnifiedStageID"] != DBNull.Value ? item["UnifiedStageID"].ToString() : null;
                    slaMng.RequestTypeID = item["RequestTypeID"] != DBNull.Value ? item["RequestTypeID"].ToString() : null;
                    slaMng.SlaGreen = item["SlaGreen"] != DBNull.Value ? item["SlaGreen"].ToString() : null;
                    slaMng.SlaYellow = item["SlaYellow"] != DBNull.Value ? item["SlaYellow"].ToString() : null;
                    slaMng.SlaRed = item["SlaRed"] != DBNull.Value ? item["SlaRed"].ToString() : null;
                    slaMng.SlaPeriodTypeID = item["SlaPeriodTypeID"] != DBNull.Value ? item["SlaPeriodTypeID"].ToString() : null;
                    slaMng.RequestTypeAgafID = item["RequestTypeAgafID"] != DBNull.Value ? item["RequestTypeAgafID"].ToString() : null;
                    slaMng.SlaStageThreshold = item["SlaStageThreshold"] != DBNull.Value ? item["SlaStageThreshold"].ToString() : null;

                }
                var FormStageIDList = a.Tables[1];
                slaMng.FormStageID = new List<ListType>();
                foreach (DataRow item2 in FormStageIDList.Rows)
                {
                    slaMng.FormStageID.Add(new ListType()
                    {
                        Id = item2["Id"] != DBNull.Value ? item2["Id"].ToString() : null,
                        Description = item2["Description"] != DBNull.Value ? item2["Description"].ToString() : null
                    });
                };
                var FormStageSelectedList = a.Tables[2];
                slaMng.FormStageSelected = new List<ListType>();
                foreach (DataRow item2 in FormStageSelectedList.Rows)
                {
                    slaMng.FormStageSelected.Add(new ListType()
                    {
                        Id = item2["Id"] != DBNull.Value ? item2["Id"].ToString() : null,
                        Description = item2["Description"] != DBNull.Value ? item2["Description"].ToString() : null
                    });
                };
                slaMngList.Add(slaMng);
                return slaMngList;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public void AddMngStageInRequest(SlaMng mh2)
        {
            if (mh2.ExistPattern == true)
            {
                List<SqlParameter> lParams = new List<SqlParameter>();
                lParams.Add(new SqlParameter("@RequestTypeID", mh2.RequestTypeID));
                lParams.Add(new SqlParameter("@SlaGreen", mh2.SlaGreen));
                lParams.Add(new SqlParameter("@SlaYellow", mh2.SlaYellow));
                lParams.Add(new SqlParameter("@SlaRed", mh2.SlaRed));
                lParams.Add(new SqlParameter("@SlaPeriodTypeID", mh2.SlaPeriodTypeID));
                lParams.Add(new SqlParameter("@RequestTypeAgafID", mh2.RequestTypeAgafID));
                lParams.Add(new SqlParameter("@SlaStageThreshold", mh2.SlaStageThreshold));
                lParams.Add(new SqlParameter("@ExistPattern", mh2.ExistPattern));
                ListModel list = new ListModel();
                try
                {
                    ExecProcedure("SP_MNG_AddMngStageInRequest", lParams);
                }
                catch (Exception ex)
                {

                }
            }
            for (int i = 0; i < mh2.FormStageID.Count(); i++)
            {
                List<SqlParameter> lParams = new List<SqlParameter>();
                lParams.Add(new SqlParameter("@UnifiedStageID", mh2.UnifiedStageID));
                lParams.Add(new SqlParameter("@FormStageID", mh2.FormStageID[i].Id));
                lParams.Add(new SqlParameter("@RequestTypeID", mh2.RequestTypeID));
                lParams.Add(new SqlParameter("@SlaGreen", mh2.SlaGreen));
                lParams.Add(new SqlParameter("@SlaYellow", mh2.SlaYellow));
                lParams.Add(new SqlParameter("@SlaRed", mh2.SlaRed));
                lParams.Add(new SqlParameter("@SlaPeriodTypeID", mh2.SlaPeriodTypeID));
                lParams.Add(new SqlParameter("@RequestTypeAgafID", mh2.RequestTypeAgafID));
                lParams.Add(new SqlParameter("@SlaStageThreshold", mh2.SlaStageThreshold));
                lParams.Add(new SqlParameter("@ExistPattern", mh2.ExistPattern));
                ListModel list = new ListModel();
                try
                {
                    ExecProcedure("SP_MNG_AddMngStageInRequest", lParams);
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void AddMngStageHisInRequest(SlaMngHis mh2)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@RequestTypeID", mh2.RequestTypeID));
            lParams.Add(new SqlParameter("@SlaHistGreen", mh2.SlaHistGreen));
            lParams.Add(new SqlParameter("@SlaHistYellow", mh2.SlaHistYellow));
            lParams.Add(new SqlParameter("@SlaHistRed", mh2.SlaHistRed));
            ListModel list = new ListModel();
            try
            {
                ExecProcedure("SP_MNG_AddMngStageHISInRequest", lParams);
            }
            catch (Exception ex)
            {

            }
        }
        public ListModel GetListTofes(string id, string type)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@Id", id));
            ListModel list = new ListModel();
            try
            {
                if (type == "1")
                {
                    var dt = ExecProcedure("SP_MNG_GetListTfasimByAgaf", lParams).Tables[0];
                    list.Tfasim = new List<ListType>();
                    //Tfasim
                    foreach (DataRow item in dt.Rows)
                    {
                        ListType tmp = new ListType();
                        tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                        tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                        list.Tfasim.Add(tmp);
                    }
                }
                else if (type == "2")
                {
                    var dt = ExecProcedure("SP_MNG_GetListStatusMeuchadByTofes", lParams).Tables[0];
                    list.StatusMeuchad = new List<ListType>();
                    //Tfasim
                    foreach (DataRow item in dt.Rows)
                    {
                        ListType tmp = new ListType();
                        tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                        tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                        list.StatusMeuchad.Add(tmp);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return list;
        }

        public ListModel GetList()
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            ListModel list = new ListModel();
            try
            {
                var dt = ExecProcedure("SP_MNG_GetList", lParams).Tables;
                list.Agafim = new List<ListType>();
                list.Tfasim = new List<ListType>();
                list.Status = new List<ListType>();
                list.StatusMeuchad = new List<ListType>();
                list.SlaPeriod = new List<ListType>();
                //Agafim
                foreach (DataRow item in dt[0].Rows)
                {
                    ListType tmp = new ListType();
                    tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                    tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                    list.Agafim.Add(tmp);
                }
                //Tfasim
                foreach (DataRow item in dt[1].Rows)
                {
                    ListType tmp = new ListType();
                    tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                    tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                    list.Tfasim.Add(tmp);
                }
                //StatusMeuchad
                foreach (DataRow item in dt[2].Rows)
                {
                    ListType tmp = new ListType();
                    tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                    tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                    list.StatusMeuchad.Add(tmp);
                }
                //SlaPeriod
                foreach (DataRow item in dt[3].Rows)
                {
                    ListType tmp = new ListType();
                    tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                    tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                    list.SlaPeriod.Add(tmp);
                }
                //Status
                foreach (DataRow item in dt[4].Rows)
                {
                    ListType tmp = new ListType();
                    tmp.Id = item["Id"] != DBNull.Value ? item["Id"].ToString() : null;
                    tmp.Description = item["Description"] != DBNull.Value ? item["Description"].ToString() : null;
                    list.Status.Add(tmp);
                }
            }
            catch (Exception ex)
            {
                //SP_MNG_GetListTfasimByAgaf
            }
            return list;
        }

        public MngForm GetByIdStg_FormMng(int id)
        {
            MngForm f = new MngForm();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            DataTable dt = null;
            try
            {
                dt = ExecProcedure("SP_MNG_GetByIdStg_FormMng", lParams).Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    f = new MngForm()
                    {
                        FormReference = dr["FormReference"] != DBNull.Value ? dr["FormReference"].ToString() : null,
                        CreateDate = dr["CreateDate"] != DBNull.Value ? dr["CreateDate"].ToString() : null,
                        SenderFirstName = dr["SenderFirstName"] != DBNull.Value ? dr["SenderFirstName"].ToString() : null,
                        SenderLastName = dr["SenderLastName"] != DBNull.Value ? dr["SenderLastName"].ToString() : null,
                        SenderMail = dr["SenderMail"] != DBNull.Value ? dr["SenderMail"].ToString() : null,
                        //FormType = dr["FormType"] != DBNull.Value ? dr["FormType"].ToString() : null,
                        FormName = dr["FormName"] != DBNull.Value ? dr["FormName"].ToString() : null,
                        SenderID = dr["SenderID"] != DBNull.Value ? dr["SenderID"].ToString() : null,
                        SenderPhone = dr["SenderPhone"] != DBNull.Value ? dr["SenderPhone"].ToString() : null,
                        //OvedEmail = dr["OvedEmail"] != DBNull.Value ? dr["OvedEmail"].ToString() : null,
                        //OvedID = dr["OvedID"] != DBNull.Value ? dr["OvedID"].ToString() : null,
                        //Notes = dr["Notes"] != DBNull.Value ? dr["Notes"].ToString() : null,
                        Name = dr["Name"] != DBNull.Value ? dr["Name"].ToString() : null,
                        //StatusID = dr["StatusID"] != DBNull.Value ? dr["StatusID"].ToString() : null,
                        DiscardSlaDesc = dr["DiscardSlaDesc"] != DBNull.Value ? dr["DiscardSlaDesc"].ToString() : null,

                    };
                }
            }
            catch (Exception e)
            {
            }
            return f;
        }

        public void UpdateMngHoliday(string id, List<MngHoliday> mh2)
        {
            try
            {
                for (int i = 0; i < mh2.Count; i++)
                {
                    List<SqlParameter> lParams = new List<SqlParameter>();
                    lParams.Add(new SqlParameter("@HYHolidayID", mh2[i].HolidayYearID));
                    lParams.Add(new SqlParameter("@HYHoliID", mh2[i].HolidayID));
                    lParams.Add(new SqlParameter("@HYHebrewYear", id));
                    if (mh2[i].HYStartDate != null && mh2[i].HYStartDate != "")
                    {
                        var arr2 = mh2[i].HYStartDate.Split('/');
                        string str = arr2[1] + '/' + arr2[0] + '/' + arr2[2];
                        lParams.Add(new SqlParameter("@HYStartDate", str));
                    }
                    if (mh2[i].HYEndDate != null && mh2[i].HYEndDate != "")
                    {
                        var arr2 = mh2[i].HYEndDate.Split('/');
                        string str = arr2[1] + '/' + arr2[0] + '/' + arr2[2];
                        lParams.Add(new SqlParameter("@HYEndDate", str));
                    }
                    ExecProcedure("SP_MNG_UpdateMngHoliday", lParams);
                }
            }
            catch (Exception EX)
            {

            }
        }
        //Get all 
        public List<MngHoliday> GetAllMngHoliday(string id)
        {
            List<MngHoliday> mngHolidayList = new List<MngHoliday>();
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@id", id));
            DataTable dt = null;
            try
            {
                dt = ExecProcedure("SP_MNG_GetAllMngHoliday", lParams).Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    mngHolidayList.Add(new MngHoliday()
                    {
                        HolidayID = dr["HolidayID"] != DBNull.Value ? dr["HolidayID"].ToString() : null,
                        HolidayName = dr["HolidayName"] != DBNull.Value ? dr["HolidayName"].ToString() : null,
                        HolidayFactor = dr["HolidayFactor"] != DBNull.Value ? dr["HolidayFactor"].ToString() : null,
                        HolidayYearID = dr["HolidayYearID"] != DBNull.Value ? dr["HolidayYearID"].ToString() : null,
                        HYHolidayID = dr["HYHolidayID"] != DBNull.Value ? dr["HYHolidayID"].ToString() : null,
                        HYStartDate = dr["HYStartDateFrmt"] != DBNull.Value ? dr["HYStartDateFrmt"].ToString() : null,
                        HYEndDate = dr["HYEndDateFrmt"] != DBNull.Value ? dr["HYEndDateFrmt"].ToString() : null,
                        HYStartDateID = dr["HYStartDateID"] != DBNull.Value ? dr["HYStartDateID"].ToString() : null,
                        HYEndDateID = dr["HYEndDateID"] != DBNull.Value ? dr["HYEndDateID"].ToString() : null,
                        HolidayFlag = dr["HolidayFlag"] != DBNull.Value ? bool.Parse(dr["HolidayFlag"].ToString()) : false,

                    });
                }
            }
            catch (Exception e)
            {
            }
            return mngHolidayList;
        }

        //add
        public void AddMngDiscardSla(MngDiscardSla mh)
        {
            try
            {
                List<SqlParameter> lParams = new List<SqlParameter>();
                lParams.Add(new SqlParameter("@DiscardSlaDesc", mh.DiscardSlaDesc));
                lParams.Add(new SqlParameter("@DiscardSlaObjectID", mh.DiscardSlaObjectID));
                lParams.Add(new SqlParameter("@DiscardSlaObjectTypeID", mh.DiscardSlaObjectTypeID));
                lParams.Add(new SqlParameter("@DiscardSlaUserID", mh.DiscardSlaUserID));
                ExecProcedure("SP_MNG_AddMngDiscardSla", lParams);
            }
            catch (Exception e)
            {
            }
        }
        public void AddMngHoliday(MngHoliday mh)
        {
            try
            {
                List<SqlParameter> lParams = new List<SqlParameter>();
                lParams.Add(new SqlParameter("@HYHolidayID", mh.HolidayName));
                if (mh.HYStartDate != null)
                    lParams.Add(new SqlParameter("@HYStartDate", new DateTime(int.Parse(mh.HYStartDate.Substring(6, 4)), int.Parse(mh.HYStartDate.Substring(3, 2)), int.Parse(mh.HYStartDate.Substring(0, 2)))));
                if (mh.HYEndDate != null)
                    lParams.Add(new SqlParameter("@HYEndDate", new DateTime(int.Parse(mh.HYEndDate.Substring(6, 4)), int.Parse(mh.HYEndDate.Substring(3, 2)), int.Parse(mh.HYEndDate.Substring(0, 2)))));
                lParams.Add(new SqlParameter("@HolidayFactor", mh.HolidayFactor));
                lParams.Add(new SqlParameter("@HYHebrewYear", mh.HYHebrewYear));

                ExecProcedure("SP_MNG_AddMngHoliday", lParams);
            }
            catch (Exception e)
            {
            }
        }

        //delete
        public void DeleteMngHoliday(int id)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@HYHolidayID", id));
            try
            {
                ExecProcedure("SP_MNG_DeleteMngHoliday", lParams);
            }
            catch (Exception e)
            {
            }
        }

        public void DeleteMngDiscardSla(int id)
        {
            List<SqlParameter> lParams = new List<SqlParameter>();
            lParams.Add(new SqlParameter("@DiscardSlaObjectID", id));
            try
            {
                ExecProcedure("SP_MNG_DeleteMngDiscardSla", lParams);
            }
            catch (Exception e)
            {
            }
        }
    }
}