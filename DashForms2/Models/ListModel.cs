﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class ListModel
    { 
        public List<ListType> Agafim { get; set; }
        public List<ListType> Tfasim { get; set; }
        public List<ListType> Status { get; set; }
        public List<ListType> StatusMeuchad { get; set; }
        public List<ListType> SlaPeriod { get; set; }
        
    }
    public class ListType
    {

        public string Id { get; set; }
        public string Description { get; set; }

    }
}