﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class MngForm
    {
        public string FormReference { get; set; }
        public string CreateDate { get; set; }
        public string SenderFirstName { get; set; }
        public string SenderLastName { get; set; }
        public string SenderMail { get; set; }
        public string FormType { get; set; }
        public string FormName { get; set; }
        public string SenderID { get; set; }
        public string SenderPhone { get; set; }
        public string OvedEmail { get; set; }
        public string OvedID { get; set; }
        public string Notes { get; set; }
        public string Name { get; set; }
        public string StatusID { get; set; }
        public string DiscardSlaDesc { get; set; }
        
    }
}