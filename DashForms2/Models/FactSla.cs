﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class FactSla
    {
        public string Id { get; set; }
        public string TypeFormId { get; set; }
        public string TypeFormName { get; set; }
        public string CreateDate { get; set; }
        public string StatusId { get; set; }
        public string UpdateDate { get; set; }
        public string SenderFirstName { get; set; }
        public string SenderLastName { get; set; }
        public string StatusName { get; set; }
        public string Sla { get; set; }
        public string SlaColor { get; set; }
        public bool IsStatusRed { get; set; }
        public bool IsStatusGreen { get; set; }
        public bool IsStatusYellow { get; set; }
        public bool IsHistStatusRed { get; set; }
        public bool IsHistStatusGreen { get; set; }
        public bool IsHistStatusYellow { get; set; }
        public string HeaderText { get; set; }
        public string SumIsReturned { get; set; } 
        
    }
        public class FactSlaShort
    { 
        public string Id { get; set; }
        public string TypeFormName { get; set; }
        public string CreateDate { get; set; }
        public string StatusName { get; set; }
        public string UpdateDate { get; set; }
        public string SenderFirstName { get; set; }
        public string SenderLastName { get; set; }
        public string HeaderText { get; set; }
        
    }
}