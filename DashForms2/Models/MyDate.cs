﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class MyDate
    {
        public string Year { get; set; }
        public string MonthName { get; set; }
        public string DayOfMonth { get; set; }
        public string enMon { get; set; }
        public string DateID { get; set; }

    }
    public class MyYechida
    {
        public string MINHALA { get; set; }
        public string TEUR_MINHALA { get; set; }
        public string AGAF { get; set; }
        public string TEUR_AGAF { get; set; }
        public string MAHLAKA { get; set; }
        public string TEUR_MAHLAKA { get; set; }
        public string YECHIDA { get; set; }
        public string TEUR_YECHIDA { get; set; }
        public string RequestTypeID { get; set; }
        public string RequestTypeName { get; set; } 
         
    }
}