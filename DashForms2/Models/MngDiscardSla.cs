﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class MngDiscardSla
    {
        public string DiscardSlaID { get; set; }
        public string DiscardSlaCreationDate { get; set; }
        public string DiscardSlaDesc { get; set; }
        public string DiscardSlaObjectID { get; set; }
        public string DiscardSlaObjectTypeID { get; set; }
        public string DiscardSlaUserID { get; set; }
       
    }
}