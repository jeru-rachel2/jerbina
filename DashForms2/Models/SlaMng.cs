﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class SlaMng
    { 
        public string UnifiedStageID { get; set; }
        public List<ListType>  FormStageSelected{ get; set; }
        public List<ListType> FormStageID { get; set; }
        public string RequestTypeID { get; set; }
        public string SlaGreen { get; set; }
        public string SlaYellow { get; set; }
        public string SlaRed { get; set; }
        public string SlaPeriodTypeID { get; set; }
        public string RequestTypeAgafID { get; set; }
        public string SlaStageThreshold { get; set; } 
        public bool ExistPattern { get; set; }

    }
    public class SlaMngHis
    {
        public string RequestTypeID { get; set; }
        public string SlaHistGreen { get; set; }
        public string SlaHistYellow { get; set; }
        public string SlaHistRed { get; set; }
        public string SlaPeriodTypeID { get; set; }
    }

    
}