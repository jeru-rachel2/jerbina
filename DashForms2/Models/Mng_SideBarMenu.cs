﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class Mng_SideBarMenu
    {
        public int Id { get; set; }
        public string Description { get; set; }
       public string stateRef { get; set; }
        public bool IsFather { get; set; }
        public int FatherGroups { get; set; }
        public bool G4200201 { get; set; }
        public bool G4200202 { get; set; }
        public bool G4200203 { get; set; }
        public bool G4200301 { get; set; }
        public bool G4200302 { get; set; }
        public bool G42004 { get; set; }
    }

}