﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class GuiScreen
    {
        public string ElementTypeName { get; set; }
        public string BiQueryDesc { get; set; }
        public string BiQueryToolTip { get; set; }
        public string BiQueryName { get; set; }
        public string BiQueryLink { get; set; }
        public string BiQueryResultFormat { get; set; } 
    }

}