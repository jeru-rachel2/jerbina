﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DashForms2.Models
{
    public class MngHoliday
    {
        public string HolidayID { get; set; }
        public string HolidayName { get; set; }
        public string HolidayFactor { get; set; }
        public string HYHolidayID { get; set; }
        public string HolidayYearID { get; set; }
        public string HYStartDate { get; set; }
        public string HYEndDate { get; set; }
        public string HYStartDateID { get; set; }
        public string HYEndDateID { get; set; }
        public string HYHebrewYear { get; set; }
        public bool  HolidayFlag { get; set; }
        

    }
}