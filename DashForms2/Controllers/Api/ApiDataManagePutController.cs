﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{
    
    public class ApiDataManagePutController : ApiController
    {
        Connect_DB cb = new Connect_DB();
        public List<MngHoliday> GetAllMngHoliday(string id)
        {
            return cb.GetAllMngHoliday(id);
        }
        [HttpPost]
        public void UpdateMngHoliday(Newtonsoft.Json.Linq.JObject mh)
        {
            string id = mh["id"].ToString();
            List<MngHoliday> mh2 = mh["mh"].ToObject<List<MngHoliday>>();
            cb.UpdateMngHoliday(id,mh2); 
        }


    }
}