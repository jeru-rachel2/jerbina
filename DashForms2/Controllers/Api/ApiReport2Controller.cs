﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{
    
    public class ApiReport2Controller : ApiController
    {
        Connect_DB cb = new Connect_DB();
        public List<FactSlaShort> GetReport2(string id, string id2,string id3,string id4,string id5)
        {
            return cb.GetReport2(id,id2,id3, id4, id5);
        } 
    }
}