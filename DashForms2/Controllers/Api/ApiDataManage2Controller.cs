﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{

    public class ApiDataManage2Controller : ApiController
    {
        Connect_DB cb = new Connect_DB();

        [HttpGet]
        public MngForm GetByIdStg_FormMng(int id,int type=0)
        {
            if(type!= 0)
            {
            cb.DeleteMngDiscardSla(id);
                return null;
            }
            return cb.GetByIdStg_FormMng(id);
        }
        [HttpPost]
        public void AddMngDiscardSla(Newtonsoft.Json.Linq.JObject mh)
        {
            MngDiscardSla mh2 = mh["id"].ToObject<MngDiscardSla>();
            cb.AddMngDiscardSla(mh2);
        }
    }
}
