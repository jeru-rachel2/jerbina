﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using DashForms2;

namespace DashForms2.Controllers.Api
{
    public class AD_Repository
    {
        DashForms2.AD_Internal.AD_WS adIn = new DashForms2.AD_Internal.AD_WS();

        public AD_Repository()
        {
            adIn.Credentials = CredentialCache.DefaultCredentials;
        }
        public List<string> GetGroups(string username, string baseGroup)
        {
            return adIn.AD_GetUserGroupsByBaseGroup(username, baseGroup).ToList();
        }
        public string GetGroupName(string GroupId)
        {
            string s = adIn.AD_GetGroupName(GroupId);
            return s;
        }
        public bool CheckUserInGroup(string userName, string groupId)
        {
            return adIn.AD_CheckUserInGroup(userName, groupId);
        }
        public List<string> GetAllReferents(string group)
        {
            List<string> names = new List<string>();
            List<string> users = adIn.AD_GetGroupUsers(group).ToList();
            foreach (string user in users)
            {
                if (user != "")
                    names.Add(adIn.AD_GetUserAttribute(user, "displayname"));
            }
            return names;
        }
        public string GetUserDisplayName(string userName)
        {
            return adIn.AD_GetUserAttribute(userName, "displayname");
        }
        public List<string> GetGroupsByBaseGroup(string baseGroup)
        {
            return adIn.AD_GetGroupsByBaseGroup(baseGroup).ToList();
        }
        public bool IsExistsHirerhy(string mainGroup)
        {
            string[] r = adIn.AD_GetGroupsByBaseGroup(mainGroup);
            return r.Length > 0;
        }
    }
}