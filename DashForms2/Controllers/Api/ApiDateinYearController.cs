﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{

    public class ApiDateinYearController : ApiController
    {
        Connect_DB cb = new Connect_DB();
        [HttpGet]
        public List<MyDate> GetDates()
        {

            return cb.GetDates();
        }
    }
}
