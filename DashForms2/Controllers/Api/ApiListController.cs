﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{

    public class ApiListController : ApiController
    {
        Connect_DB cb = new Connect_DB();
        [HttpGet]
        public ListModel GetList(string id=null,string type=null)
        {
            if(id!=null)
                return cb.GetListTofes(id,type);
            return cb.GetList(); 
        } 
    }
}
