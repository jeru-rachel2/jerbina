﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;
//using System.Web.Script.Serialization;

namespace DashForms2.Controllers.Api
{

    public class ApiLoginController : ApiController
    {
        [HttpGet]
        public Dictionary<string, string> CheckUser(string id)
        {
            AD_Internal.AD_WS AD_Service = new AD_Internal.AD_WS();
            Dictionary<string, string> user = new Dictionary<string, string>();
            string sIDM = ConfigurationSettings.AppSettings["BaseGroupNo"];
            bool InBaseGroup = false;
            AD_Service.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            if (id == "anatania")
                id = "NETANIA";
            List<string> Groups = new List<string>(string.Join(",", ((string[])AD_Service.AD_GetUserGroupsByBaseGroup(id, sIDM))).Split(','));
            if (Groups.Count > 0 && Groups[0] != String.Empty)
            {
                user.Add("user_groups",  Groups[0]);
                user.Add("display_name", AD_Service.AD_GetUserAttribute(id, "displayName"));
            }
            else
            {
                return null;
            } 
            return user;
        }
    }
}

