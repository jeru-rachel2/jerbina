﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{
    
    public class ApiDataManageController : ApiController
    {
        Connect_DB cb = new Connect_DB();
      
        [HttpGet]
        public void GetByIdMngHoliday(int id)
        {
            cb.DeleteMngHoliday(id);
        }
        [HttpPost]
        public void AddMngHoliday(Newtonsoft.Json.Linq.JObject mh)
        {
            MngHoliday mh2 = mh["id"].ToObject<MngHoliday>();
            cb.AddMngHoliday(mh2);
        }
       
        
    }
}