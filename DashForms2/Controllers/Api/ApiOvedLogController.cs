﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{

    public class ApiOvedLogController : ApiController
    {
        Connect_DB cb = new Connect_DB();

        [HttpGet]
        public List<Enteries> Fact_OvedLogGetAll(string id=null)
        {
           return cb.Fact_OvedLogGetAll(id);
        }
        [HttpPost]
        public void Fact_OvedLogInsert(string id)
        {
            var arr = id.Split(',');
            var userName = arr[0];
            var screen = arr[1];
            cb.Fact_OvedLogInsert(userName, screen);
        }

    }
}