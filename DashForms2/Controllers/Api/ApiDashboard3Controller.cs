﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{

    public class ApiDashboard3Controller : ApiController
    {
        Connect_DB cb = new Connect_DB();

        public List<FactSla> GetAllDashboard3(string id, string id2, string id3)
        {
            if (id2 != "-1" && id3=="1")
                return cb.GetAllDashboard3ToStatus(id, id2);
            else if (id2 != "-1" && id3=="2")
                return cb.GetAllDashboard3ToType(id, id2);
            return cb.GetAllDashboard3(id);
        }

        public Myclass GetDate()
        {
            return cb.GetDate();
        }
    }
}
