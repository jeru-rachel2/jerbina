﻿using DashForms2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security.Principal;
using System.Text;
//using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace DashForms2.Controllers.Api
{
    public class ApiCreateSessionController : ApiController
    {
        Random random = new Random((int)DateTime.Now.Ticks);
        void AssignReferentToForm(int formId, string refName)
        {
            //var context = new TFASIM_MEKUVANIMEntities();
            using (var context = new Entities())
            {

                var form = context.Forms.Where(f => f.FormReference == formId).FirstOrDefault();

                if (form == null)
                {
                    return;
                }

                else
                {
                    form.Official = refName;
                    context.SaveChanges();
                }
            }
        }

        private string RandomString(int Size)
        {
            string input = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var chars = Enumerable.Range(0, Size).Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }
        private string CreateSession(int reference)
        {
            var context = new Entities();
            Sessions ss = new Sessions();
            ss.SessionKey = RandomString(20);
            ss.ID = reference;
            context.Sessions.Add(ss);
            context.SaveChanges();
            return ss.SessionKey;
        }
        public string GetSession(string id = null)
        {
            AD_Repository ad = new AD_Repository();
            string user = ad.GetUserDisplayName(Environment.UserName);
            //שיוך רפרנט לטופס
            //if (!isManager)
            int iid = Convert.ToInt32(id);
            AssignReferentToForm(iid, user);
            var context = new Entities();
            try
            {
                FormAccess fa = context.FormAccess.Where(f => f.FormReference == iid).FirstOrDefault();
                int? FormType = context.Forms.Where(f => f.FormReference == iid).FirstOrDefault() != null ? context.Forms.Where(f => f.FormReference == iid).FirstOrDefault().FormType : null;
                FormsMng form = context.FormsMng.Where(a => a.ID == FormType).FirstOrDefault() != null ? context.FormsMng.Where(a => a.ID == FormType).FirstOrDefault() : null;
                string url = null;
                if (form != null)
                    url = form.PathURL;
                string session = CreateSession(iid);
                string nav = url + (string.IsNullOrEmpty(fa.Params) ? ("?sess=") : (fa.Params + "&sess=")) + session + "&mng=true&username=" + user.Replace(" ", "_") + ("&agaf=" + form.IdmGroup);
                //System.Diagnostics.Process.Start(nav);
                return nav;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

    }
}