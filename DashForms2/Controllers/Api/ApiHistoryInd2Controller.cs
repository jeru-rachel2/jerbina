﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{

    public class ApiHistoryInd2Controller : ApiController
    {
        Connect_DB cb = new Connect_DB();
        [HttpGet]
        public SlaMngHis GetMngStage2InRequestById(String id,string id2=null)
        {
            if (id2 == null)
                return cb.GetMngStage2InRequestById(id);
            else
            {
                cb.DeleteMngStage2InRequest(id);
                return null;
            }
        }
        [HttpPost]
        public void AddMngStageHisInRequest(Newtonsoft.Json.Linq.JObject mh)
        {
            SlaMngHis mh2 = mh["id"].ToObject<SlaMngHis>();
            cb.AddMngStageHisInRequest(mh2);
        } 
    }
}
