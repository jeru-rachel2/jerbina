﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DashForms2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace DashForms2.Controllers.Api
{ 
    public class ApiHistoryIndController : ApiController
    {
        Connect_DB cb = new Connect_DB();

        [HttpGet]
        public List<SlaMng> GetMngStageInRequestById(String id)
        {
           return cb.GetMngStageInRequestById(id);
        }
        [HttpPost]
        public string AddMngStageInRequest(Newtonsoft.Json.Linq.JObject mh)
        {
            try
            {
                SlaMng mh2 = mh["mh"].ToObject<SlaMng>();
            cb.AddMngStageInRequest(mh2);
            }
            catch (Exception ex)
            {

            }
            return "";
        }  
    }
}
