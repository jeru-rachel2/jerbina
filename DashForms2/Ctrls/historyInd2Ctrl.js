(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('historyInd2Ctrl', historyInd2Ctrl);
    function historyInd2Ctrl(OvedLogService,$scope, Translate, HistoryInd2Service, ListService, Notification) {
        $scope.Item = {};
        var stateRef = "historyInd2"
        var WinNetWork = new ActiveXObject("WScript.Network");
        var temp = WinNetWork.UserName + "," + stateRef
        OvedLogService.post({ id: temp });
        ListService.get(function (data) {
            $scope.Tfasim = data.Tfasim; 
            //$scope.RequestTypeID = $scope.Tfasim[0].Id;
            $scope.$$childTail.RequestTypeID = null;
            $scope.Item = {};
        });
        $scope.Save = function () {
            if ($scope.$$childTail.RequestTypeID == null || $scope.$$childTail.RequestTypeID == "" || $scope.$$childTail.RequestTypeID == undefined) {
                $scope.error(Translate("msg18"));
                return;
            }
            if ($scope.Item.SlaHistGreen == null || $scope.Item.SlaHistRed == null || $scope.Item.SlaHistGreen == undefined || $scope.Item.SlaHistRed == undefined) {
                $scope.error(Translate("msg17"));
                return;
            }
            else if ($scope.Item.SlaHistGreen < $scope.Item.SlaHistRed) {
                $scope.Item.RequestTypeID = $scope.$$childTail.RequestTypeID;
                HistoryInd2Service.post({ id: $scope.Item }).$promise.then(function (data) {
                    $scope.success(Translate("msg2"));
                }, function (error) {
                    $scope.error(Translate("msg3"));
                });
            }
            else
                $scope.error(Translate("msg1"));
        }
        $scope.Cancel = function () {
            $scope.$$childTail.RequestTypeID = null;
            //$scope.$$childTail.RequestTypeID = $scope.Tfasim[0].Id;
            $scope.Item = {};
        }
        $scope.ChangeTofes = function () {

            HistoryInd2Service.get({ id: $scope.$$childTail.RequestTypeID }, function (data) {
                $scope.Item = data;
                if ($scope.Item.SlaHistGreen != null )
                    $scope.Item.SlaHistGreen = parseInt($scope.Item.SlaHistGreen);
                //if ($scope.Item.SlaHistYellow != null )
                //    $scope.Item.SlaHistYellow = parseInt($scope.Item.SlaHistYellow);
                if ($scope.Item.SlaHistRed != null )
                    $scope.Item.SlaHistRed = parseInt($scope.Item.SlaHistRed);
            });
            //    ListService.get({ id: $scope.$$childTail.$$childTail.Agaf }, function (data) {
            //        $scope.Tfasim = data.Tfasim;
            //    });
        }
        $scope.error = function (msg) {
            Notification.error({ message: msg, delay: 1500 });
        };
        $scope.success = function (msg) {
            Notification.success({ message: msg, delay: 1500 });
        };





        $scope.remove = function () {
            if ($scope.$$childTail.RequestTypeID == null || $scope.$$childTail.RequestTypeID == "" || $scope.$$childTail.RequestTypeID == undefined) {
                $scope.error(Translate("msg18"));
                return;
            }
            else {
                $scope.successHtml4();
            }
        }
        $scope.removeYes = function () {
            HistoryInd2Service.get({ id: $scope.$$childTail.RequestTypeID,id2:"1" })
                .$promise.then(function () {
                    $scope.success(Translate("msg2"));
                    $scope.Item = {};
                }, function (error) {
                    $scope.error(Translate("msg3"));
                });
        }
        $scope.warning = function (msg) {
            Notification.warning({ message: msg, delay: 1500 });
        };
        $scope.successHtml4 = function () {
            Notification.success({ message: '<div><label>' + Translate("msg5") + '</label></br><button id="btnPrepend4"  class="form-control" style="display: inline; max-width: 60px; margin-top: 10px;   max-height: 25px;   padding: 5px;text-align: center; " >' + Translate("msg9") + '</button><button  class="form-control" style=" margin-left: 45px;display: inline;max-width: 60px; margin-top: 10px;   max-height: 25px;   padding: 5px;text-align: center; " >' + Translate("msg10") + '</button></div>', delay: 150000 });
        };
        $(document).on('click', '#btnPrepend4', function () {
            $scope.removeYes();
        }); 
    }
})();



