(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('report2Ctrl', report2Ctrl);
    function report2Ctrl(OvedLogService, Report2Service, $window, ListService, $scope, Translate, $location) {
        var stateRef = "report"
        var WinNetWork = new ActiveXObject("WScript.Network");
        var temp = WinNetWork.UserName + "," + stateRef
        OvedLogService.post({ id: temp });
        var arr = decodeURI($location.$$url.replace($location.$$path, '').replace('?', '')).split(";");
        $scope.divLoading = false;
        $scope.FormType = arr[0];
        $scope.CurrentStatus = arr[3];
        $scope.Back = function (id) {
            $location.url("/report?" + $scope.SelectedDate)
        }
        var tempArr = arr[1].split(',');
        for (var i = 0; i < tempArr.length; i++) {
            if (tempArr[i] != "" && tempArr[i].split('%')[1].length == 3)
                tempArr[i] = tempArr[i].replace("%2F", "/0")
            else
                tempArr[i] = tempArr[i].replace("%2F", "/")
        }
        arr[1] = tempArr.join();
        $scope.SelectedDate = arr[1];
        ListService.get(function (data) {
            $scope.StatusMeuchad = data.StatusMeuchad;
            $scope.StatusMeuchad.push({ Description: Translate('report7'), Id: -1 });
            $scope.Show();
        });
        //begin excel
        var dataTable = [];
        var cols = [];
        $scope.bigFrom500 = false;
        $scope.ExcelExportData = function () {
            Report2Service.query({ 'id': $scope.FormType, 'id2': $scope.$$childTail.CurrentStatus, 'id3': $scope.SelectedDate, 'id4': "2", 'id5': arr[3] }, function (data) {
                $scope.tableData = data;
            
            if ($scope.tableData == null || $scope.tableData.length == 0) {
                $window.alert(Translate('ExcelExportDataNull'));
                return;
            }
            cols = [Translate('th1'), Translate('th2'), Translate('th3'), Translate('th4'), Translate('th7'), Translate('th14'), Translate('th15')];
            var repTitlte = Translate('m2');
            var table = '<table border="1" ><thead><tr><th colspan="9"><h3> ' + repTitlte + '</h3><h3> ' + $scope.SelectedDate + '</h3></th></tr><tr>';
            for (var i = 0; i < cols.length; i++) {
                table += '<th style="background:blue;color:#fff">' + cols[i] + '</th>';
            }
            table += '</tr></thead><tbody>';
            for (var i = 0; i < $scope.tableData.length; i++) {
                table += '<tr>';
                var data = $scope.tableData[i];
                table += '<td   >' + data.Id + '</td>';
                table += '<td   >' + data.TypeFormName + '</td>';
                table += '<td   >' + data.CreateDate + '</td>';
                table += '<td   >' + data.StatusName + '</td>';
                table += '<td   >' + data.UpdateDate + '</td>';
                table += '<td   >' + data.SenderFirstName + '</td>';
                table += '<td   >' + data.SenderLastName + '</td>';
                table += '</tr>';
            }
            table += '</tbody></table>';
            var blob = new Blob([table], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Report.xls");
            });
        };
        $scope.Show = function () {
            $scope.divLoading = true;
            Report2Service.query({ 'id': $scope.FormType, 'id2': $scope.$$childTail.CurrentStatus, 'id3': $scope.SelectedDate, 'id4': "1", 'id5': arr[3] }, function (data) {
                $scope.divLoading = false;
                if (data.length == 500)
                    $scope.bigFrom500 = true;
                else
                    $scope.bigFrom500 = false;

                $scope.tableData = data
            });
        }
        $scope.ChangeStatus = function () {
            $scope.Show();
        }

    }
})();