(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('generalReportCtrl', generalReportCtrl);
    function generalReportCtrl(GeneralReportService, OvedLogService, $window, $scope, $timeout, baConfig, Translate, LevelsChartP3Service, StatusChartP3Service) {
        var stateRef = "generalReport"
        var WinNetWork = new ActiveXObject("WScript.Network");
        var temp = WinNetWork.UserName + "," + stateRef
        OvedLogService.post({ id: temp });
        $scope.dataBar = [];
        $scope.labelsBar = [];
        $scope.seriesBar = [];
        $scope.dataBar2 = [];
        $scope.labelsBar2 = [];
        $scope.seriesBar2 = [];
        OvedLogService.query({ id: null }, function (data) {
            for (var i = 0; i < data.length; i++) {
                $scope.dataBar[i] = data[i].AgafCnt;
            }
            for (var i = 0; i < data.length; i++) {
                $scope.labelsBar[i] = data[i].Agaf;
            }
            for (var i = 0; i < data.length; i++) {
                $scope.seriesBar[i] = data[i].AgafCnt;
            }
        });
        GeneralReportService.query({ id: null }, function (data) {
            for (var i = 0; i < data.length; i++) {
                $scope.dataBar2[i] = data[i].AgafCnt;
            }
            for (var i = 0; i < data.length; i++) {
                $scope.labelsBar2[i] = data[i].Agaf;
            }
            for (var i = 0; i < data.length; i++) {
                $scope.seriesBar2[i] = data[i].AgafCnt;
            }
        });


        $scope.changeDateOved = function () {
            var ovedFrom = null;
            var ovedTo = null;
            if ($('#ovedFrom') != null && $('#ovedFrom')[0] != null && $('#ovedFrom')[0].value != "")
                ovedFrom = $('#ovedFrom')[0].value;
            if ($('#ovedTo') != null && $('#ovedTo')[0] != null && $('#ovedTo')[0].value != "")
                ovedTo = $('#ovedTo')[0].value;
            var date = ovedFrom + "," + ovedTo;
            date = date.replace('/', '-')
            date = date.replace('/', '-')
            date = date.replace('/', '-')
            date = date.replace('/', '-')
            OvedLogService.query({ id: date }, function (data) {
                 $scope.dataBar = [];
        $scope.labelsBar = [];
        $scope.seriesBar = []; 
                for (var i = 0; i < data.length; i++) {
                    $scope.dataBar[i] = data[i].AgafCnt;
                }
                for (var i = 0; i < data.length; i++) {
                    $scope.labelsBar[i] = data[i].Agaf;
                }
                for (var i = 0; i < data.length; i++) {
                    $scope.seriesBar[i] = data[i].AgafCnt;
                }
            });
        };
        $scope.changeDateDiscard = function () {
           var a= $("#discardFrom").data("");
            var discardFrom = null;
            var discardTo = null;
            if ($('#discardFrom') != null && $('#discardFrom')[0] != null && $('#discardFrom')[0].value != "")
                discardFrom = $('#discardFrom')[0].value;
            if ($('#discardTo') != null && $('#discardTo')[0] != null && $('#discardTo')[0].value != "")
                discardTo = $('#discardTo')[0].value;
            var date = discardFrom + "," + discardTo;
            date = date.replace('/', '-')
            date = date.replace('/', '-')
            date = date.replace('/', '-')
            date = date.replace('/', '-')
            GeneralReportService.query({ id: date }, function (data) { 
                $scope.dataBar2 = [];
                $scope.labelsBar2 = [];
                $scope.seriesBar2 = [];
                for (var i = 0; i < data.length; i++) {
                    $scope.dataBar2[i] = data[i].AgafCnt;
                }
                for (var i = 0; i < data.length; i++) {
                    $scope.labelsBar2[i] = data[i].Agaf;
                }
                for (var i = 0; i < data.length; i++) {
                    $scope.seriesBar2[i] = data[i].AgafCnt;
                }
            });
        };
    }
})();