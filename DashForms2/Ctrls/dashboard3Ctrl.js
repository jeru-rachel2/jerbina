(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('dashboard3Ctrl', dashboard3Ctrl);
    function dashboard3Ctrl(OvedLogService, $http, $q, DashService, GuiScreenElementService, LoginService, baUtil, $scope, Translate, CreateSessionService, Dashboard3Service, $window, $timeout, baConfig, LevelsChartP3Service, StatusChartP3Service) {
        var stateRef = "dashboard3";
        $(document).on('click', "#panel1-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(document).on('click', "#panel2-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(document).on('click', "#panel3-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(document).on('click', "#panel4-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(function () {
            var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
            $scope.Colors2 = ['#ed6573', '#9551b5', '#e1f4ad', '#149b7e', '#C71585', '#FF1493', '#FF69B4', '#7FFFD4', '#5F9EA0', ];
            $scope.Colors = ['#d4027f', '#6ed0f7', '#fa9d26', '#5991cc', '#f0474a', '#94b60a']
            $scope.GuiScreenElement(4);
            setInterval(function () {
                Dashboard3Service.get(function (data) {
                    $scope.Title2 = " ";
                    $scope.Title2 += data.str;
                });
                $scope.ChangeUser();
            }, 30 * 60 * 1000)
            //$.connection.hub.url = "http://jer-bina-tsta/signalr";
            //$.connection.hub.url = "/signalr";
            //var dashnotify = $.connection.cubeHub;
            //dashnotify.client.broadcastMessage = function (name, message) {
            //    var encodedName = $('<div />').text(name).html();
            //    if (encodedName == "etlServer") {
            //        $scope.ChangeUser();
            //        var encodedName = $('<div />').text(name).html();
            //        var encodedMsg = $('<div />').text(message).html();
            //        $('#discussion').append('<li><strong>' + encodedName
            //            + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
            //    };
            //}
            //$.connection.hub.start();
        });
        var vm = this;
        vm.user = {};
        $scope.data = {};
        var WinNetWork = new ActiveXObject("WScript.Network");
        vm.user.userName = WinNetWork.UserName;
        var temp = WinNetWork.UserName + "," + stateRef
        //vm.user.userName = 'hyadi'; 
        $scope.userName = {};
        $scope.userNameSelected = {};
        $scope.userNames = [{ id: 'CODANA', Name: Translate('user1') },
         { id: 'annoa', Name: Translate('user2') },
         { id: 'hyadi', Name: Translate('user5') },
         { id: 'shavital', Name: Translate('user6') },
         { id: 'shbar', Name: Translate('user7') },
         { id: 'anatania', Name: Translate('user8') },
         { id: 'BrUri1', Name: Translate('user9') },
         { id: 'brrachel2', Name: Translate('user10') },
        { id: 'SMAYELET', Name: Translate('user11') },
        ];
        $scope.userName = vm.user.userName;
        $scope.Title2 = ''
        $scope.labels = [];
        $scope.data = [];
        $scope.labelsBar = [];
        $scope.seriesBar = [];
        $scope.dataBar = [];
        $scope.metricsTableData = [];
        // var url = 'https://jer-binaa.muni.jerusalem.muni.il/api/v1.0/get_res_query_name/to_object/';
        //var url = 'http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/';
       var url = 'http://localhost:50920/api/v1.0/get_res_query_name/to_object/';
        OvedLogService.post({ id: temp });
        $scope.GuiScreenElement = function (id) {
            GuiScreenElementService.query({ 'id': id }, function (data) {
                $scope.dashChart = data;
                $scope.channels = [];
                $scope.infos = [];
                for (var i = 0; i < $scope.dashChart.length; i++) {
                    $scope.channels[i] = $scope.dashChart[i].BiQueryName;
                }
                $scope.fullLinks = $scope.channels.map(function (channel) {
                    return url + channel + '/' + vm.user.userName;
                });
                $q.all($scope.fullLinks.map(function (url) {
                    return $http.get(url).success(function (data) {
                        $scope.infos.push({ key: data.rowData[0][0], value: data.columnData[0] });
                    });
                })).then(function () {
                    for (var i = 0; i < $scope.dashChart.length; i++) {
                        for (var j = 0; j < $scope.infos.length; j++) {
                            if ($scope.dashChart[i].BiQueryName == $scope.infos[j].value) {
                                if ($scope.dashChart[i].BiQueryResultFormat == "1" && $scope.infos[j].key.indexOf('.') == -1)
                                    $scope.dashChart[i].perc = $scope.infos[j].key;
                                else if ($scope.dashChart[i].BiQueryResultFormat == "2")
                                    $scope.dashChart[i].perc = parseInt(parseFloat($scope.infos[j].key) * 100) + "%";
                                else if ($scope.dashChart[i].BiQueryResultFormat == "2" && $scope.infos[j].key == "0")
                                    $scope.dashChart[i].perc = $scope.infos[j].key + "%";
                                break;
                            }
                        }
                        $scope.dashChart[i].background = { 'background-color': $scope.Colors[i] };
                    }
                });
            })
        }
        $scope.ChangeUser = function (item) {
            if ($scope.userNameSelected.slc != null)
                vm.user.userName = $scope.userNameSelected.slc;
            $scope.Login();
            $scope.GuiScreenElement(4);
            $scope.LevelsChartP3();
            $scope.StatusChartP3();
        }
        $scope.Login = function () {
            LoginService.get({ id: vm.user.userName }, function (data) {
                $scope.data.selected = data.user_groups;
                $scope.display_name = data.display_name;
                if ($scope.display_name == null || $scope.display_name == "") {
                    $window.alert(Translate('NotMorshe'));
                    return;
                }
                localStorage.setItem('userName', vm.user.userName);
                localStorage.setItem('display_name', data.display_name);
                localStorage.setItem('group', $scope.data.selected);
                if (data.user_groups == '42001' || data.user_groups == '4200201')
                    $scope.Idm = data.user_groups;
                Dashboard3Service.query({ 'id': $scope.display_name, 'id2': "-1", id3: "1" }, function (data) {
                    $scope.metricsTableData = data;
                    //$("#myTable").update();
                    //var sorting = [[1, 0]];
                    //setTimeout(function () {
                    //    $("#myTable").trigger('sorton', [sorting])
                    //    if ($("#myTable").find('tbody:first tr').length > 0) {
                    //        $("#myTable").tablesorter({
                    //            dateFormat: "uk",
                    //            theme: 'bootstrap',
                    //            headerTemplate: '{content}{icon}',
                    //        });
                    //    }
                    //}, 10);
                })
            });
        }
        $scope.Clean = function () {
            Dashboard3Service.query({ 'id': $scope.display_name, 'id2': "-1", id3: "1" }, function (data) {
                $scope.metricsTableData = data;
            })
        }
        $scope.grafClickEvent = function (points, evt) {
            Dashboard3Service.query({ 'id': $scope.display_name, 'id2': points[0]._model.label, id3: "1" }, function (data) {
                $scope.metricsTableData = data;
            })
        }
        $scope.graf2ClickEvent = function (points, evt) {
            Dashboard3Service.query({ 'id': $scope.display_name, 'id2': points[0]._model.label, id3: "2" }, function (data) {
                $scope.metricsTableData = data;
            })
        }
        Dashboard3Service.get(function (data) {
            $scope.Title2 = " ";
            $scope.Title2 += data.str;
        });
        $scope.LevelsChartP3 = function () {
            $scope.labels = [];
            $scope.data = [];
            LevelsChartP3Service.get({ 'id': vm.user.userName }, function (data) {
                $scope.data2 = [];
                var j = 0;
                if (data.rowData) {
                    for (var i = 0; i < data.rowData.length; i++) {
                        if (data.rowData[i][1] != "0") {
                            $scope.data2[j] = {};
                            $scope.data2[j].value = data.rowData[i][1];
                            $scope.data2[j].Desc = data.rowData[i][0];
                            j++;
                        }
                    }
                    AmCharts.addInitHandler(function (chart) {
                        if (chart.legend === undefined || chart.legend.truncateLabels === undefined)
                            return;

                        // init fields
                        var titleField = chart.titleField;
                        var legendTitleField = chart.titleField + "Legend";

                        // iterate through the data and create truncated label properties
                        for (var i = 0; i < chart.dataProvider.length; i++) {
                            var label = chart.dataProvider[i][chart.titleField];
                            if (label.length > chart.legend.truncateLabels)
                                label = label.substr(0, chart.legend.truncateLabels - 1) + '...'
                            chart.dataProvider[i][legendTitleField] = label;
                        }

                        // replace chart.titleField to show our own truncated field
                        chart.titleField = legendTitleField;

                        // make the balloonText use full title instead
                        chart.balloonText = chart.balloonText.replace(/\[\[title\]\]/, "[[" + titleField + "]]");

                    }, ["pie"]);
                    var chart = AmCharts.makeChart("chartdiv6", {
                        "startDuration": 0,
                        "type": "pie",
                        "theme": "light",
                        "labelsEnabled": false,
                        autoMargins: false,
                        marginTop: 0,
                        marginBottom: 0,
                        marginLeft: 0,
                        marginRight: 0,
                        pullOutRadius: 0,
                        "legend": {
                            "markerType": "circle",
                            "position": "right",
                            "marginRight": 20,
                            "autoMargins": false,
                            "truncateLabels": 20// custom parameter
                        },
                        "dataProvider": $scope.data2,
                        "valueField": "value",
                        "titleField": "Desc",
                        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                        "listeners": [{
                            "event": "clickSlice",
                            "method": function (event) {
                                Dashboard3Service.query({ 'id': $scope.display_name, 'id2': event.dataItem.legendTextReal, id3: "2" }, function (data) {
                                    $scope.metricsTableData = [];
                                    $scope.metricsTableData = data;

                                })
                            }
                        }]
                    });
                }
            });
        }
        $scope.StatusChartP3 = function () {
            $scope.seriesBar = [];
            $scope.labelsBar = [];
            $scope.dataBar = [];
            StatusChartP3Service.get({ 'id': vm.user.userName }, function (data) {
                $scope.data2 = [];
                $scope.data2Color = ["#FF9E01", "#F8FF01", "#FF6600", "#FCD202", "#B0DE09", "#04D215", "#0D8ECF", "#0D52D1", "#2A0CD0", "#8A0CCF", "#CD0D74"];
                var j = 0;
                if (data.columnData)
                    for (var i = 0; i < data.columnData.length; i++) {
                        $scope.seriesBar[i] = data.columnData[i];
                    }
                if (data.rowData) {
                    for (var i = 0; i < data.rowData.length; i++) {
                        $scope.data2[j] = {};
                        $scope.data2[j].country = data.rowData[i][0];
                        $scope.data2[j].visits = data.rowData[i][1];
                        $scope.data2[j].color = $scope.data2Color[j];
                        j++;
                        //$scope.labelsBar[i] = data.rowData[i][0];
                        //$scope.dataBar[i] = data.rowData[i][1];
                    }
                    var chart = AmCharts.makeChart("chartdiv4", {
                        "type": "serial",
                        "theme": "light",
                        "marginRight": 0,
                        "dataProvider": $scope.data2,

                        "gridAboveGraphs": true,
                        "valueAxes": [{
                            "axisAlpha": 0.2,
                            "id": "v1",
                            "minimum": 0,
                            //"maximum": 5000
                        }],
                        "startDuration": 0,
                        "graphs": [{
                            "balloonText": "<b>[[category]]: [[value]]</b>",
                            "fillColorsField": "color",
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "visits",
                            "labelText": "[[visits]]",
                        }
                        ],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "fullWidth": true,
                            "cursorAlpha": 0.1,
                            "zoomable": false,
                            "listeners": [{
                                "event": "changed",
                                "method": function (ev) {
                                    // Log last cursor position
                                    ev.chart.lastCursorPosition = ev.index;
                                }
                            }]
                        },
                        "categoryField": "country",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0,
                            "tickPosition": "start",
                            "tickLength": 20,
                            "labelRotation": 45
                        },
                        "listeners": [{
                            "event": "init",
                            "method": function (ev) {
                                // Set a jQuery click event on chart area
                                jQuery(ev.chart.chartDiv).on("click", function (e) {
                                    // Check if last cursor position is present
                                    if (!isNaN(ev.chart.lastCursorPosition)) {
                                        Dashboard3Service.query({ 'id': $scope.display_name, 'id2': ev.chart.dataProvider[ev.chart.lastCursorPosition].country, id3: "1" }, function (data) {
                                            $scope.metricsTableData = [];
                                            $scope.metricsTableData = data;
                                        });
                                    }
                                });
                            }
                        }]
                    });
                }
            }, function (data) {
            });
        };
        $scope.FormWatchClick = function (id) {
            //CreateSessionService.get({ id: id }).then(function (data) {
            //    window.open(data.data, '_blank')
            //}, function (error) {
            //});
        };

        function process(date) {
            var parts = date.split("/");
            var a = parts[2].split(" ")[1];
            var aa1 = a.split(":");
            return new Date(parts[2].split(" ")[0], parts[1] - 1, parts[0], aa1[0], aa1[1]).valueOf();
        }
        $scope.sortTable = function sortTable(n) {
            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("myTable");
            switching = true;
            // Set the sorting direction to ascending:
            dir = "asc";
            /* Make a loop that will continue until
            no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.getElementsByTagName("TR");
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1) ; i++) {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    /* Check if the two rows should switch place,
                    based on the direction, asc or desc: */
                    if (dir == "asc") {
                        var isDate = new Date(x.innerHTML.toLowerCase()) !== "Invalid Date" && !isNaN(new Date(x.innerHTML.toLowerCase())) ? true : false;
                        if (!isDate && x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                        else if (isDate && process(x.innerHTML.toLowerCase()) > process(y.innerHTML.toLowerCase())) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                    else if (dir == "desc") {



                        var isDate = new Date(x.innerHTML.toLowerCase()) !== "Invalid Date" && !isNaN(new Date(x.innerHTML.toLowerCase())) ? true : false;
                        if (!isDate && x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                        else if (isDate && process(x.innerHTML.toLowerCase()) < process(y.innerHTML.toLowerCase())) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    // Each time a switch is done, increase this count by 1:
                    switchcount++;
                } else {
                    /* If no switching has been done AND the direction is "asc",
                    set the direction to "desc" and run the while loop again. */
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }

        $scope.Login();
        $scope.LevelsChartP3();
        $scope.StatusChartP3();
    };
})();