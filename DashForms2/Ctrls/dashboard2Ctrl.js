(function () {
    'use strict';
    angular.module('BlurAdmin.pages.charts.chartist').controller('dashboard2Ctrl', dashboard2Ctrl);
    function dashboard2Ctrl(OvedLogService,LoginService, Dashboard3Service, $http, $q, DashService, GuiScreenElementService, DashboardLineService, DashboardDoughnutService, DashboardBarService, DashboardPieService, DateinYearService, $scope, Translate, $window) {
        var stateRef = "dashboard2";
      
        var treedata_avm = [];
        var StatusArray = ['', '', '', '', '', Translate('status5'), Translate('status6'), Translate('status7'), Translate('status8'), Translate('status9'), '', Translate('status11'), Translate('status12'), Translate('status13')];
        $scope.Colors = ['#d4027f', '#6ed0f7', '#fa9d26', '#5991cc', '#f0474a', '#94b60a', '#4661EE', '#EC5657', '#1BCDD1', '#8FAABB', '#B08BEB', '#3EA0DD', '#F5A52A', '#23BFAA', '#FAA586', '#EB8CC6', ]
        var ctx = null, ctx2 = null, myBarChart = null, myBarChart2 = null, stackedLine = null;
        $scope.colours = ['#EC5657', '#1BCDD1', '#8FAABB', '#B08BEB', '#3EA0DD', '#F5A52A', '#23BFAA', '#FAA586', '#EB8CC6', ]
        var url = 'http://jer-bina-tsta/api/v1.0/get_res_query_name/to_object/';
        // var url = 'http://localhost:50920/api/v1.0/get_res_query_name/to_object/';
        var vm = this;
        vm.user = {};
        $scope.data = {};
        var WinNetWork = new ActiveXObject("WScript.Network");
        vm.user.userName = WinNetWork.UserName;
        var temp = WinNetWork.UserName + "," + stateRef
        OvedLogService.post({ id: temp });
        DateinYearService.query(function (data) {
            $scope.Dates = data;
            var year = null;
            var MonthName = null;
            var j = -1;
            var k = 0;
            var l = 0;
            treedata_avm = [{ label: Translate('All'), children: [] }];
            for (var i = 0; i < data.length; i++) {
                if (data[i].Year != year) {
                    year = data[i].Year;
                    j++;
                    treedata_avm[0].children[j] = {};
                    treedata_avm[0].children[j].label = year;
                    treedata_avm[0].children[j].DateID = year;
                    treedata_avm[0].children[j].checked = false;
                    treedata_avm[0].children[j].children = [];
                    k = 0;
                }
                if (data[i].MonthName != MonthName) {
                    MonthName = data[i].MonthName;
                    k++;
                    treedata_avm[0].children[j].children[k] = {};
                    treedata_avm[0].children[j].children[k].DateID = data[i].DateID;
                    treedata_avm[0].children[j].children[k].checked = false;
                    treedata_avm[0].children[j].children[k].label = data[i].MonthName;
                    treedata_avm[0].children[j].children[k].label2 = data[i].enMon;
                }
            }
            $scope.my_data = treedata_avm;
            $scope.my_data[0].children[3].children[2].checked = true;
            //treedata_avm[0].checked = false;
        })
        $scope.my_tree_handler = function (branch) {
            var _ref;
            if (branch != null && branch.level == 2 && branch.checked == false) {
                $scope.my_data[0].checked = false
                $scope.once2 = $scope.once2.replace(".&[" + branch.label + "]", "");
                if ($scope.once2.substring($scope.once2.length - 1, $scope.once2.length) == ',')
                    $scope.once2 = $scope.once2.substring(0, $scope.once2.length - 1)
            }
            if (branch != null && branch.level == 1) {

                if ($scope.my_data != null && $scope.my_data[0].children != null)
                    for (var i = 0; i < $scope.my_data[0].children.length; i++) {
                        if ($scope.my_data[0].checked && $scope.my_data[0].children[i] != null)
                            $scope.my_data[0].children[i].checked = true;
                        else if ($scope.my_data[0].children[i] != null)
                            $scope.my_data[0].children[i].checked = false;
                        for (var j = 0; j < $scope.my_data[0].children[i].children.length; j++)
                            if ($scope.my_data[0].checked && $scope.my_data[0].children[i].children[j] != null)
                                $scope.my_data[0].children[i].children[j].checked = true;
                            else if ($scope.my_data[0].children[i].children[j] != null)
                                $scope.my_data[0].children[i].children[j].checked = false;
                    }
            }
            else if ($scope.my_data != null && $scope.my_data[0].children) {
                for (var i = 0; i < $scope.my_data[0].children.length; i++) {
                    if (branch != null && branch.DateID == $scope.my_data[0].children[i].DateID && $scope.my_data[0].children[i].children != null)
                        for (var j = 0; j < $scope.my_data[0].children[i].children.length; j++) {
                            if ($scope.my_data[0].children[i].checked && $scope.my_data[0].children[i].children[j] != null)
                                $scope.my_data[0].children[i].children[j].checked = true;
                            else if ($scope.my_data[0].children[i].children[j] != null)
                                $scope.my_data[0].children[i].children[j].checked = false;

                        }
                }
            }
        };
        $scope.Show = function (once) {
            $scope.chartdiv2Level1 = false;
            $scope.chartdiv2Level2 = false;
            $scope.chartdiv2Level0 = true;
            $scope.myChart2Level1 = false;
            $scope.myChart2Level0 = true;
            $scope.chartlineLevel1 = false;
            $scope.chartlineLevel0 = true;
            if (document.getElementById("myChart"))
                ctx = document.getElementById("myChart").getContext("2d");
            var currentDate = new Date();
            var data5 = {}, EndData = {}
            $scope.labels = [], $scope.labels2 = [], $scope.data = [], $scope.data2 = [];
            if (once == 1) {
                var Month = parseInt(currentDate.getUTCMonth() + 1);
                $scope.dat = ".&[" + currentDate.getFullYear() + "][" + Month + "]";
                //$scope.dat = ".&[2017][1]"; 
            }
            else {
                $scope.dat = "";
                if ($scope.my_data && $scope.my_data[0].checked == true)
                    $scope.dat = ".&AllMembers"
                else if ($scope.my_data) {
                    for (var i = 0; i < $scope.my_data[0].children.length; i++) {
                        for (var j = 0; j < $scope.my_data[0].children[i].children.length; j++) {
                            if ($scope.my_data[0].children[i].children[j] && $scope.my_data[0].children[i].children[j].checked == true) {
                                if ($scope.dat != "")
                                    $scope.dat += ",";
                                var year = $scope.my_data[0].children[i].children[j].DateID.substring(0, 4);
                                var mon = $scope.my_data[0].children[i].children[j].DateID.substring(4, 6);
                                if (mon.substring(0, 1) == "0")
                                    mon = mon.substring(1, 2)
                                $scope.dat += ".&[" + year + "]" + "[" + mon + "]";
                            }
                        }
                    }
                }

                if ($scope.dat == "") {
                    //$scope.dat = ".&[2017][1]";
                    var Month = parseInt(currentDate.getUTCMonth() + 1);
                    $scope.dat = ".&[" + currentDate.getFullYear() + "][" + Month + "]";
                }

                if (stackedLine != null && myBarChart != null) {
                    stackedLine.destroy();
                    myBarChart.destroy();
                }
            }
            if ($scope.dat != "")
                $scope.divLoading = true;
            var send = '{"Date":"' + $scope.dat + '", "OrgRequest":"' + $scope.org + '"}';
            var dates = ".&[2017][1],.&[2017][2],.&[2017][3],.&[2017][4],.&[2017][5],.&[2017][6],.&[2017][7],.&[2017][8],.&[2017][9],.&[2017][10],.&[2017][11],.&[2017][12],.&[2018][1]"
            var dates = ".&[2017][1],.&[2017][2],.&[2017][3],.&[2017][4],.&[2017][5],.&[2017][6],.&[2017][7],.&[2017][8],.&[2017][9],.&[2017][10],.&[2017][11],.&[2017][12],.&[2018][1]"
            if (dates.indexOf($scope.dat) == -1)
                dates += ',' + $scope.dat;
            var send2 = '{"Date":"' + dates + '", "OrgRequest":"' + $scope.org + '"}';
            $scope.colors = ["#B0DE09", "#04D215", "#0D8ECF", "#0D52D1", "#2A0CD0", "#8A0CCF", "#CD0D74", "#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01"]


            DashboardBarService.post({ 'id': send }, function (data2) {
                ctx = document.getElementById("myChart").getContext("2d");
                var j = 0, k = 0, subs = [], divs = [], labels = []
                for (var i = 1; i < data2.columnData.length; i++) {
                    if (parseInt(data2.rowData[1][i]) < parseInt(data2.rowData[0][i]) && parseInt(data2.rowData[0][i]) != 0)
                        divs[i - 1] = parseInt((parseInt(data2.rowData[1][i]) / parseInt(data2.rowData[0][i])) * 100);
                    else if (parseInt(data2.rowData[1][i]) != 0)
                        divs[i - 1] = parseInt((parseInt(data2.rowData[0][i]) / parseInt(data2.rowData[1][i])) * 100);
                    subs[i - 1] = 100 - divs[i - 1];
                    labels[i - 1] = StatusArray[parseInt(data2.columnData[i].substring(1, data2.columnData[i].length))];
                }
                EndData = {
                    labels: labels,
                    datasets: [{
                        label: Translate("Status7"),
                        backgroundColor: "rgba(122, 214, 128, 1)",
                        borderColor: "rgba(122, 214, 128, 1)",
                        data: subs//[sub5, sub4, sub3, sub2, sub1]
                    },
                    {
                        label: Translate("Status6"),
                        backgroundColor: "rgba(222, 135, 135, 1)",
                        borderColor: "rgba(222, 135, 135, 1)",
                        data: divs//[div5, div4, div3, div2, div1]
                    }
                    ]
                };
                if (myBarChart) {
                    myBarChart.destroy();
                }
                myBarChart = new Chart(ctx, {
                    type: 'bar',
                    data: EndData,

                    options: {
                        tooltips: {
                            titleFontSize: 13,
                            titleFontFamily: "ASSISTANT",
                            callbacks: {
                                label: function (tooltipItem) {
                                    return tooltipItem.yLabel + "%";
                                }
                            }
                        },
                        scales: {
                            xAxes: [
                                {
                                    labelAngle: 0,
                                    stacked: true,
                                    ticks: {
                                        fontSize: 15,
                                        fontFamily: "ASSISTANT",
                                    },
                                },
                            ],
                            yAxes: [{
                                labelAngle: 0,
                                stacked: true,
                                ticks: {
                                    min: 0,
                                    max: 100,
                                    callback: function (value) { return value + "%" },
                                    beginAtZero: true,
                                    fontSize: 15,
                                    fontFamily: "ASSISTANT",
                                },
                                scaleLabel: {
                                    display: true,
                                }
                            }]
                        },
                        onClick: function (c, i) {
                            $scope.myChart2Level0 = false;
                            $scope.myChart2Level1 = true;
                            var x_value = this.data.labels[i[0]._index];
                            $scope.DrillParam1 = StatusArray.indexOf(x_value);
                            var send3 = ' {"Date":"' + $scope.dat + '", "OrgRequest":"' + $scope.org + '", "DrillParam"' + ':".&[' + $scope.DrillParam1 + ']", "DrillLevel":"1"}';
                            DashboardBarService.post({ 'id': send3 }, function (data7) {
                                var ctx2 = document.getElementById("myChart2").getContext("2d");
                                var j = 0, k = 0, subs2 = [], divs2 = [], labels2 = []
                                for (var i = 1; i < data7.columnData.length; i++) {
                                    if (parseInt(data7.rowData[1][i]) < parseInt(data7.rowData[0][i]) && parseInt(data7.rowData[0][i]) != 0)
                                        divs2[i - 1] = parseInt((parseInt(data7.rowData[1][i]) / parseInt(data7.rowData[0][i])) * 100);
                                    else if (parseInt(data7.rowData[1][i]) != 0)
                                        divs2[i - 1] = parseInt((parseInt(data7.rowData[0][i]) / parseInt(data7.rowData[1][i])) * 100);
                                    subs2[i - 1] = 100 - divs2[i - 1];
                                    if ($scope.my_data2 != null) {
                                        for (var k = 0; k < $scope.my_data2[0].children.length; k++) {
                                            if (parseInt($scope.my_data2[0].children[k].DateID) == parseInt(data7.columnData[i].substring(1, data7.columnData[i].length))) {
                                                labels2[i - 1] = $scope.my_data2[0].children[k].label;
                                                break;
                                            }
                                        }
                                    }
                                }
                                EndData = {
                                    labels: labels2,
                                    datasets: [{
                                        label: Translate("Status7"),
                                        backgroundColor: "rgba(122, 214, 128, 1)",
                                        borderColor: "rgba(122, 214, 128, 1)",
                                        data: subs2//[sub5, sub4, sub3, sub2, sub1]
                                    },
                                    {
                                        label: Translate("Status6"),
                                        backgroundColor: "rgba(222, 135, 135, 1)",
                                        borderColor: "rgba(222, 135, 135, 1)",
                                        data: divs2//[div5, div4, div3, div2, div1]
                                    }
                                    ]
                                };
                                if (myBarChart2) {
                                    myBarChart2.destroy();
                                }
                                myBarChart2 = new Chart(ctx2, {
                                    type: 'bar',
                                    data: EndData,
                                    options: {
                                        tooltips: {
                                            titleFontSize: 13,
                                            titleFontFamily: "ASSISTANT",
                                            callbacks: {
                                                label: function (tooltipItem) {
                                                    return tooltipItem.yLabel + "%";
                                                }
                                            }
                                        },

                                        scales: {
                                            xAxes: [
                                                {
                                                    labelAngle: 0,
                                                    stacked: true,
                                                    ticks: {
                                                        fontSize: 15,
                                                        fontFamily: "ASSISTANT",
                                                    },
                                                },
                                            ],
                                            yAxes: [{
                                                labelAngle: 0,
                                                stacked: true,
                                                ticks: {
                                                    min: 0,
                                                    max: 100,
                                                    callback: function (value) { return value + "%" },
                                                    beginAtZero: true,
                                                    fontSize: 15,
                                                    fontFamily: "ASSISTANT",
                                                },
                                                scaleLabel: {
                                                    display: true,
                                                }
                                            }]
                                        }
                                    },

                                });
                            });

                        },
                    },

                });
            });
            if (once == 1)
                $scope.once2 = ".&[" + currentDate.getFullYear() + "]";
            else {
                function onlyUnique(value, index, self) {
                    return self.indexOf(value) === index;
                }

                var arr = $scope.dat.split('.&[').toString().split(']');
                var unique = arr.filter(onlyUnique);
                for (var j = 0; j < unique.length; j++) {
                    if ($scope.my_data) {
                        for (var i = 0; i < $scope.my_data[0].children.length; i++) {
                            if (unique[j].indexOf($scope.my_data[0].children[i].label) !== -1 && $scope.once2.indexOf($scope.my_data[0].children[i].label) == -1)
                                $scope.once2 += ",.&[" + $scope.my_data[0].children[i].label + "]";
                        }
                    }
                }
            }
            function labelFunction(info) {
                var data = info.dataContext;
                return data.value;
            }
            DashboardLineService.post({ 'id': send2 }, function (data2) {
                var dataDate = [];
                for (var i = 0; i < data2.rowData.length; i++) {
                    dataDate[i] = {};

                    dataDate[i].date = Translate(data2.rowData[i][1]) + ' ' + data2.rowData[i][0];
                    dataDate[i].opened = data2.rowData[i][2];
                    dataDate[i].open = data2.rowData[i][3];
                    dataDate[i].itsOk = parseInt(data2.rowData[i][4]) + parseInt(data2.rowData[i][5]) + parseInt(data2.rowData[i][6]);
                    //dataDate[i].posted = data2.rowData[i][5];
                    //dataDate[i].returned = data2.rowData[i][6];
                }
                var chart = AmCharts.makeChart("chartdiv3", {
                    pathToImages: 'assets/img/theme/vendor/amcharts/dist/amcharts/images/',
                    "type": "serial",
                    "theme": "light",
                    "balloon": {
                        fontSize: 13
                    },
                    "precision": 2,
                    "valueAxes": [{
                        "id": "v1",
                        "title": "",
                        "position": "left",
                        "autoGridCount": false,
                        "labelFunction": function (value) {
                            return value;
                        }
                    }],
                    "graphs": [{
                        "id": "g3",
                        "valueAxis": "",
                        "lineColor": "#fdd400",
                        "fillColors": "#fdd400",
                        "fillAlphas": 1,
                        "type": "column",
                        "title": Translate('Count In Treatment'),
                        "valueField": "open",
                        "clustered": false,
                        "columnWidth": 0.5,
                        "legendValueText": "[[value]]",
                        "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
                    }, {
                        "id": "g4",
                        "valueAxis": "v1",
                        "lineColor": "#62cf73",
                        "fillColors": "#62cf73",
                        "fillAlphas": 1,
                        "type": "column",
                        "title": Translate('Status3'),
                        "valueField": "itsOk",
                        "clustered": false,
                        "columnWidth": 0.3,
                        "legendValueText": "[[value]]",
                        "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
                    }, {
                        "id": "g1",
                        "valueAxis": "v2",
                        "bullet": "round",
                        "bulletBorderAlpha": 1,
                        "bulletColor": "#FFFFFF",
                        "bulletSize": 5,
                        "hideBulletsCount": 50,
                        "lineThickness": 2,
                        "lineColor": "#20acd4",
                        "type": "smoothedLine",
                        "title": Translate('New Requests'),
                        "useLineColorForBulletBorder": true,
                        "valueField": "opened",
                        "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
                    }],
                    "chartScrollbar": {
                        "graph": "g1",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 30,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": false,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2,
                        "listeners": [{
                            "event": "changed",
                            "method": function (ev) {
                                ev.chart.cursorDataContext = ev.chart.dataProvider[ev.index];
                            }
                        }]
                    },
                    "numberFormatter": {
                        "precision": -1,
                        "decimalSeparator": ",",
                        "thousandsSeparator": ""
                    },
                    "categoryField": "date",
                    "categoryAxis": {
                        "parseDates": false,
                        "dashLength": 1,
                        "minorGridEnabled": true,
                        labelRotation: 35
                    },
                    "legend": {
                        "position": "right",
                        "divId": "legenddiv3"
                    },
                    "dataProvider": dataDate,
                    "addClassNames": true,
                    "listeners": [
                        {
                            "event": "clickGraphItem",
                            "method": function (e) {
                                $scope.chartlineLevel0 = false;
                                $scope.chartlineLevel1 = true;
                                var status = e.item.graph.legendTextReal;
                                var mon = Translate(e.item.category.split(' ')[1]);
                                var year = e.item.category.split(' ')[2];
                                $scope.DrillParam = year + "][" + mon;

                                $scope.DrillParam1 = Translate(status);
                                var send3 = '';
                                if ($scope.DrillParam1 != '8')
                                    send3 = '{"Date":"' + dates + '",  "OrgRequest":"' + $scope.org + '", "DrillParam"' + ':".&[' + $scope.DrillParam1 + ']' + '", "DrillParam1"' + ':".&[' + $scope.DrillParam + ']", "DrillLevel":"1"}';
                                else
                                    send3 = '{"Date":"' + dates + '", "OrgRequest":"' + $scope.org + '", "DrillParam"' + ':".&[8],.&[11],.&[12]' + '", "DrillParam1"' + ':".&[' + $scope.DrillParam + ']", "DrillLevel":"1"}';
                                DashboardLineService.post({ 'id': send3 }, function (data2) {
                                    var dataDate = [];
                                    var j = 0;
                                    for (var i = 0; i < data2.rowData.length; i++) {
                                        if (((data2.rowData[i].length <= 2 && data2.rowData[i][1] != "" && data2.rowData[i][1] != "0")
                                            || (data2.rowData[i].length > 2 && data2.rowData[i][2] != "" && data2.rowData[i][2] != "0")
                                            || (data2.rowData[i].length > 2 && data2.rowData[i][3] != "" && data2.rowData[i][3] != "0")
                                            )
                                             && data2.rowData[i][0] != null) {
                                            dataDate[j] = {};
                                            dataDate[j].label = data2.rowData[i][0];
                                            if (data2.rowData[i].length > 2)
                                                dataDate[j].sum = (data2.rowData[i][1] != "" ? parseInt(data2.rowData[i][1]) : 0) + (data2.rowData[i][2] != "" ? parseInt(data2.rowData[i][2]) : 0) + (data2.rowData[i][3] != "" ? parseInt(data2.rowData[i][3]) : 0);
                                            else
                                                dataDate[j].sum = data2.rowData[i][1];
                                            j++;
                                        }
                                    }
                                    var chart = AmCharts.makeChart("chartdiv3Level1", {
                                        pathToImages: 'assets/img/theme/vendor/amcharts/dist/amcharts/images/',
                                        "type": "serial",
                                        //"theme": "light",
                                        "balloon": {
                                            fontSize: 13
                                        },
                                        //"graphs": [{
                                        //    "id": "g1",
                                        //    "valueAxis": "g1",
                                        //    "lineColor": "#fdd400",
                                        //    "fillColors": "#fdd400",
                                        //    "fillAlphas": 1,
                                        //    "type": "column",
                                        //    "title": '',
                                        //    "valueField": "sum",
                                        //    "clustered": false,
                                        //    "columnWidth": 0.5,
                                        //    "legendValueText": "[[value]]",
                                        //    "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
                                        //}],
                                        "dataProvider": dataDate,
                                        "valueAxes": [{
                                            "gridColor": "#FFFFFF",
                                            "gridAlpha": 0.2,
                                            "dashLength": 0
                                        }],
                                        "gridAboveGraphs": true,
                                        "startDuration": 0,
                                        "graphs": [{
                                            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>",
                                            "labelText": "[[value]]",
                                            "fillAlphas": 0.8,
                                            "lineAlpha": 0.2,
                                            "type": "column",
                                            "valueField": "sum"
                                        }],
                                        "chartCursor": {
                                            "categoryBalloonEnabled": false,
                                            "cursorAlpha": 0,
                                            "zoomable": false
                                        },
                                        "categoryField": "label",
                                        "categoryAxis": {
                                            "gridPosition": "start",
                                            "gridAlpha": 0,
                                            labelRotation: 35
                                        }
                                    });
                                });
                            }
                        }, ]
                });
            });
            DashboardPieService.post({ 'id': send }).$promise.then(function (data) {
                $scope.data2 = [];
                var j = 0;
                if (data.rowData) {
                    for (var i = 0; i < data.rowData[0].length; i++) {
                        if (data.rowData[0][i] != "0") {
                            $scope.data2[j] = {};
                            $scope.data2[j].value = data.rowData[0][i]
                            $scope.data2[j].Desc = StatusArray[parseInt(data.columnData[i].substring(1, data.columnData[i].length))]
                            j++;
                        }
                    }
                    if ($scope.chartdiv2 != null)
                        $scope.chartdiv2.clear();
                    $scope.chartdiv2 = null;
                    $scope.chartdiv2 = AmCharts.makeChart("chartdiv2", {
                        "labelText": "[[percents]]%",
                        //labelsEnabled: false,
                        autoMargins: false,
                        marginTop: 20,
                        marginBottom: 0,
                        marginLeft: 0,
                        marginRight: 0,
                        pullOutRadius: 0,
                        "balloon": {
                            fontSize: 13
                        },
                        "labelRadius": 23,
                        "type": "pie",
                        "theme": "light",
                        "startDuration": 0,
                        "dataProvider": $scope.data2,
                        "valueField": "value",
                        "titleField": "Desc",
                        "addClassNames": true,
                        "legend": {
                            "position": "left",
                            "marginRight": 100,
                            "autoMargins": false,
                            "divId": "legenddiv2"
                        },
                        "defs": {
                            "filter": [{
                                "id": "shadow",
                                "width": "200%",
                                "height": "200%",
                                "feOffset": {
                                    "result": "offOut",
                                    "in": "SourceAlpha",
                                    "dx": 0,
                                    "dy": 0
                                },
                                "feGaussianBlur": {
                                    "result": "blurOut",
                                    "in": "offOut",
                                    "stdDeviation": 5
                                },
                                "feBlend": {
                                    "in": "SourceGraphic",
                                    "in2": "blurOut",
                                    "mode": "normal"
                                }
                            }]
                        },
                        "listeners": [{
                            "event": "clickSlice",
                            "method": function (event) {
                                $scope.chartdiv2Level2 = false;
                                $scope.chartdiv2Level0 = false;
                                $scope.chartdiv2Level1 = true;
                                $scope.DrillParam1 = StatusArray.indexOf(event.dataItem.legendTextReal);
                                var send3 = ' {"Date":"' + $scope.dat + '", "OrgRequest":"' + $scope.org + '", "DrillParam"' + ':".&[' + $scope.DrillParam1 + ']", "DrillLevel":"1"}';
                                DashboardPieService.post({ 'id': send3 }).$promise.then(function (data5) {
                                    $scope.data3 = [];
                                    if (data5.rowData) {
                                        for (var i = 0; i < data5.rowData.length; i++) {
                                            //if (data5.rowData[i][1] != "0") {
                                            $scope.data3[i] = {};
                                            $scope.data3[i].value = data5.rowData[i][1]
                                            $scope.data3[i].Desc = data5.rowData[i][0]
                                            $scope.data3[i].color = $scope.colors[i]
                                            j++;
                                            //}
                                        }
                                        AmCharts.makeChart("chartdiv2Level11", {
                                            "balloon": {
                                                fontSize: 13,
                                                "enabled": false
                                            },
                                            "type": "serial",
                                            "theme": "none",
                                            "dataProvider": $scope.data3,
                                            "marginRight": 70,
                                            "gridAboveGraphs": true,
                                            "startDuration": 0,
                                            "graphs": [{
                                                "balloonText": "<b>[[category]]: [[value]]</b>",
                                                "fillColorsField": "color",
                                                "fillAlphas": 0.9,
                                                "lineAlpha": 0.2,
                                                "type": "column",
                                                "valueField": "value",
                                                "labelText": "[[value]]",
                                            }],
                                            "chartCursor": {
                                                "fullWidth": true,
                                                "cursorAlpha": 0.1,
                                                "listeners": [{
                                                    "event": "changed",
                                                    "method": function (ev) {
                                                        // Log last cursor position
                                                        ev.chart.lastCursorPosition = ev.index;
                                                    }
                                                }]
                                            },
                                            "categoryField": "Desc",
                                            "categoryAxis": {
                                                "gridPosition": "start",
                                                "gridAlpha": 0,
                                                "tickPosition": "start",
                                                "tickLength": 20,
                                                "labelRotation": 23.4
                                            },
                                            "valueAxes": [{
                                                "axisAlpha": 0.2,
                                                "id": "v1",
                                                "minimum": 0,
                                                //"maximum": 5000
                                            }],
                                            "listeners": [{
                                                "event": "init",
                                                "method": function (ev) {
                                                    // Set a jQuery click event on chart area
                                                    jQuery(ev.chart.chartDiv).on("click", function (e) {
                                                        // Check if last cursor position is present
                                                        if (!isNaN(ev.chart.lastCursorPosition)) {
                                                            $scope.chartdiv2Level1 = false;
                                                            $scope.chartdiv2Level0 = false;
                                                            $scope.chartdiv2Level2 = true;
                                                            if ($scope.my_data2) {
                                                                for (var i = 0; i < $scope.my_data2[0].children.length; i++) {
                                                                    if ($scope.my_data2[0].children[i].label == ev.chart.dataProvider[ev.chart.lastCursorPosition].Desc) {
                                                                        $scope.DrillParam = $scope.my_data2[0].children[i].DateID;
                                                                        break;
                                                                    }
                                                                }

                                                                var send3 = ' {"Date":"' + $scope.dat + '", "OrgRequest":"' + $scope.org + '", "DrillParam"' + ':".&[' + $scope.DrillParam1 + ']' + '", "DrillParam1"' + ':".&[' + $scope.DrillParam + ']", "DrillLevel":"2"}';
                                                                DashboardPieService.post({ 'id': send3 }).$promise.then(function (data6) {
                                                                    $scope.data4 = [];
                                                                    if (data6.rowData) {
                                                                        for (var i = 0; i < data6.rowData.length; i++) {
                                                                            $scope.data4[i] = {};
                                                                            $scope.data4[i].value = data6.rowData[i][1]
                                                                            $scope.data4[i].Desc = data6.rowData[i][0]
                                                                            $scope.data4[i].color = $scope.colors[i]
                                                                            j++;
                                                                        }
                                                                        AmCharts.makeChart("chartdiv2Level2", {
                                                                            "type": "serial",
                                                                            "theme": "none",
                                                                            "dataProvider": $scope.data4,
                                                                            "marginRight": 70,
                                                                            "gridAboveGraphs": true,
                                                                            "startDuration": 0,
                                                                            "graphs": [{
                                                                                "balloonText": "<b>[[category]]: [[value]]</b>",
                                                                                "fillColorsField": "color",
                                                                                "fillAlphas": 0.9,
                                                                                "lineAlpha": 0.2,
                                                                                "type": "column",
                                                                                "valueField": "value",
                                                                                "labelText": "[[value]]",
                                                                            }],
                                                                            "balloon": {
                                                                                fontSize: 13,
                                                                                "enabled": false
                                                                            },
                                                                            "chartCursor": {
                                                                                "categoryBalloonEnabled": false,
                                                                                "cursorAlpha": 0,
                                                                                "zoomable": false
                                                                            },
                                                                            "categoryField": "Desc",
                                                                            "categoryAxis": {
                                                                                "gridPosition": "start",
                                                                                "gridAlpha": 0,
                                                                                "tickPosition": "start",
                                                                                "tickLength": 20,
                                                                                "labelRotation": 23.4
                                                                            },
                                                                            "valueAxes": [{
                                                                                "axisAlpha": 0.2,
                                                                                "id": "v1",
                                                                                "minimum": 0,
                                                                                //"maximum":1000
                                                                            }],
                                                                        });
                                                                    }
                                                                }, function (error) { });
                                                            }
                                                        }
                                                    });
                                                }
                                            }]
                                            //"labelText": "[[percents]]%",
                                            //autoMargins: false,
                                            //marginTop: 20,
                                            //marginBottom: 0,
                                            //marginLeft: 0,
                                            //marginRight: 0,
                                            //pullOutRadius: 0,
                                            //"labelRadius": 23, 
                                            //"valueField": "value",
                                            //"titleField": "Desc",
                                            //"addClassNames": true,

                                            //"legend": {
                                            //    "position": "left",
                                            //    "marginRight": 100,
                                            //    "autoMargins": false,
                                            //    "divId": "legenddiv2Level1"
                                            //}, 
                                        });
                                    }
                                }, function (error) { });
                            }
                        }]
                    });
                }
            }, function (error) { });
            DashboardDoughnutService.post({ 'id': send }, function (data) {
                var j = 0;
                $scope.data4 = [];
                if (data.rowData) {
                    for (var i = 0; i < data.rowData.length; i++) {
                        if (data.rowData[i][1] != "0") {
                            $scope.data4[j] = {};
                            $scope.data4[j].value = data.rowData[i][1]
                            $scope.data4[j].Desc = data.rowData[i][0]
                            $scope.labels2[j] = data.rowData[i][0];
                            $scope.data2[j] = data.rowData[i][1];
                            j++;
                        }
                    }
                    var chart = AmCharts.makeChart("chartdiv7", {
                        labelsEnabled: false,
                        autoMargins: false,
                        marginTop: 0,
                        marginBottom: 0,
                        marginLeft: 0,
                        marginRight: 0,
                        pullOutRadius: 0,
                        "type": "pie",
                        "theme": "none",
                        "startDuration": 0,
                        "radius": "42%",
                        "innerRadius": "60%",
                        "dataProvider": $scope.data4,
                        "valueField": "value",
                        "titleField": "Desc",
                        "addClassNames": true,
                        "legend": {
                            "markerType": "circle",
                            "position": "right",
                            "marginRight": 40,
                            "autoMargins": false,
                            "truncateLabels": 30 // custom parameter
                        },
                        "defs": {
                            "filter": [{
                                "id": "shadow",
                                "width": "200%",
                                "height": "200%",
                                "feOffset": {
                                    "result": "offOut",
                                    "in": "SourceAlpha",
                                    "dx": 0,
                                    "dy": 0
                                },
                                "feGaussianBlur": {
                                    "result": "blurOut",
                                    "in": "offOut",
                                    "stdDeviation": 5
                                },
                                "feBlend": {
                                    "in": "SourceGraphic",
                                    "in2": "blurOut",
                                    "mode": "normal"
                                }
                            }]
                        },
                        "listeners": [{
                            "event": "clickSlice",
                            "method": function (event) {
                                $scope.chartdiv7Level2 = false;
                                $scope.chartdiv7Level0 = false;
                                $scope.chartdiv7Level1 = true;
                                if ($scope.my_data2) {
                                    for (var i = 0; i < $scope.my_data2[0].children.length; i++) {
                                        if ($scope.my_data2[0].children[i].label == event.dataItem.legendTextReal) {
                                            $scope.DrillParam1 = $scope.my_data2[0].children[i].DateID;
                                            break;
                                        }
                                    }
                                    var send3 = ' {"Date":"' + $scope.dat + '", "OrgRequest":"' + $scope.org + '", "DrillParam"' + ':".&[' + $scope.DrillParam1 + ']", "DrillLevel":"1"}';
                                    DashboardDoughnutService.post({ 'id': send3 }, function (data5) {
                                        var j = 0;
                                        $scope.data11 = [];
                                        if (data5.rowData) {
                                            for (var i = 0; i < data5.rowData.length; i++) {
                                                if (data5.rowData[i][1] != "0") {
                                                    $scope.data11[j] = {};
                                                    $scope.data11[j].value = data5.rowData[i][1]
                                                    $scope.data11[j].Desc = data5.rowData[i][0]
                                                    j++;
                                                }
                                            }
                                            AmCharts.makeChart("chartdiv7Level1", {
                                                labelsEnabled: false,
                                                autoMargins: false,
                                                marginTop: 0,
                                                marginBottom: 0,
                                                marginLeft: 0,
                                                marginRight: 0,
                                                pullOutRadius: 0,
                                                "type": "pie",
                                                "theme": "none",
                                                "startDuration": 0,
                                                "radius": "42%",
                                                "innerRadius": "60%",
                                                "dataProvider": $scope.data11,
                                                "valueField": "value",
                                                "titleField": "Desc",
                                                "addClassNames": true,
                                                "legend": {
                                                    "markerType": "circle",
                                                    "position": "right",
                                                    "marginRight": 40,
                                                    "autoMargins": false,
                                                    "truncateLabels": 30 // custom parameter
                                                },
                                                "defs": {
                                                    "filter": [{
                                                        "id": "shadow",
                                                        "width": "200%",
                                                        "height": "200%",
                                                        "feOffset": {
                                                            "result": "offOut",
                                                            "in": "SourceAlpha",
                                                            "dx": 0,
                                                            "dy": 0
                                                        },
                                                        "feGaussianBlur": {
                                                            "result": "blurOut",
                                                            "in": "offOut",
                                                            "stdDeviation": 5
                                                        },
                                                        "feBlend": {
                                                            "in": "SourceGraphic",
                                                            "in2": "blurOut",
                                                            "mode": "normal"
                                                        }
                                                    }]
                                                },
                                                //"listeners": [{
                                                //    "event": "clickSlice",
                                                //    "method": function (event) {
                                                //        $scope.chartdiv7Level0 = false;
                                                //        $scope.chartdiv7Level1 = false;
                                                //        $scope.chartdiv7Level2 = true;
                                                //        if ($scope.my_data2) {
                                                //            for (var i = 0; i < $scope.my_data2[0].children.length; i++) {
                                                //                for (var j = 0; j < $scope.my_data2[0].children[i].children.length; j++) {
                                                //                    for (var k = 0; k < $scope.my_data2[0].children[i].children[j].children.length; k++) {
                                                //                        if ($scope.my_data2[0].children[i].children[j].children[k].label == event.dataItem.legendTextReal) {
                                                //                            $scope.DrillParam1 = $scope.my_data2[0].children[i].DateID;
                                                //                            break;
                                                //                        }
                                                //                    }
                                                //                }
                                                //            }
                                                //            DashboardService.query({ 'id': $scope.DrillParam1 }, function (data5) {

                                                //            });
                                                //        }
                                                //    }
                                                //}],
                                                "balloon": {
                                                    fontSize: 13
                                                }
                                            });
                                        }
                                    }, function (error) { });
                                }
                            }
                        }],
                        "balloon": {
                            fontSize: 13
                        }


                    });
                    chart.addListener("init", handleInit);
                    chart.addListener("rollOverSlice", function (e) {
                        handleRollOver(e);
                    });
                    function handleInit() {
                        chart.legend.addListener("rollOverItem", handleRollOver);
                    }
                    function handleRollOver(e) {
                        var wedge = e.dataItem.wedge.node;
                        wedge.parentNode.appendChild(wedge);
                    }
                }

            });
            $scope.GuiScreenElement(6);
        }

        $(function () {
            $scope.chartdiv2Level1 = false;
            $scope.chartdiv2Level2 = false;
            $scope.chartdiv2Level0 = true;
            $scope.chartdiv7Level1 = false;
            $scope.chartdiv7Level2 = false;
            $scope.chartdiv7Level0 = true;
            $scope.myChart2Level1 = false;
            $scope.myChart2Level0 = true;
            $scope.chartlineLevel1 = false;
            $scope.chartlineLevel0 = true; 
            LoginService.get({ id: vm.user.userName }, function (data) {
                $scope.data.selected = data.user_groups;
                $scope.display_name = data.display_name;
                $scope.MINHALA = data.MINHALA;
                $scope.AGAF = data.AGAF;
                if ($scope.MINHALA == -1 || $scope.AGAF == -1)
                    $scope.org = ".&ALLMEMBERS";
                else
                    $scope.org = ".&[" + $scope.MINHALA + "][" + $scope.AGAF + "]";
                if ($scope.display_name == null || $scope.display_name == "") {
                    $window.alert(Translate('NotMorshe'));
                    return;
                }
                ctx = document.getElementById("myChart").getContext("2d");
                $scope.Show(1);
                $scope.branch = {};
                $scope.branch.level = 1;
                setInterval(function () {
                    Dashboard3Service.get(function (data) {
                        $scope.Title2 = " ";
                        $scope.Title2 += data.str;
                    });
                    $scope.my_tree_handler();
                    $scope.Show();
                }, 60 * 60 * 1000)
                $scope.my_tree_handler($scope.branch)

            });

            //$.connection.hub.url = "http://jer-bina-tsta/signalr";
            //$.connection.hub.url = "/signalr";
            //var dashnotify = $.connection.cubeHub;
            //dashnotify.client.broadcastMessage = function (name, message) {
            //    var encodedName = $('<div />').text(name).html();
            //    if (encodedName == "etlServer") {
            //        $scope.my_tree_handler();
            //        var encodedMsg = $('<div />').text(message).html();
            //        $('#discussion').append('<li><strong>' + encodedName
            //            + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
            //    };
            //}
            //$.connection.hub.start();
        })
        $(document).on('click', "#panel1-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(document).on('click', "#panel2-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(document).on('click', "#panel3-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });
        $(document).on('click', "#panel4-fullscreen", function (e) {
            e.stopImmediatePropagation();
            var $this = $(this);
            if ($this.children('i').hasClass('glyphicon-resize-full')) {
                $this.children('i').removeClass('glyphicon-resize-full');
                $this.children('i').addClass('glyphicon-resize-small');
            }
            else if ($this.children('i').hasClass('glyphicon-resize-small')) {
                $this.children('i').removeClass('glyphicon-resize-small');
                $this.children('i').addClass('glyphicon-resize-full');
            }
            $(this).closest('.panel').toggleClass('panel2-fullscreen');
        });

        $(document).on('click', ".Show", function (e) {
            e.stopImmediatePropagation();
            $scope.chartdiv2Level1 = false;
            $scope.chartdiv2Level2 = false;
            $scope.chartdiv2Level0 = true;
            $scope.chartlineLevel1 = false;
            $scope.chartlineLevel0 = true;
            $scope.Show();
        });
        $scope.GuiScreenElement = function (id) {
            GuiScreenElementService.query({ 'id': id }, function (data) {
                $scope.dashChart = data;
                $scope.channels = [];
                $scope.infos = [];
                for (var i = 0; i < $scope.dashChart.length; i++) {
                    $scope.channels[i] = $scope.dashChart[i].BiQueryName;
                }
                $scope.fullLinks = $scope.channels.map(function (channel) {
                    return url + channel;
                });
                if ($scope.org == "")
                    $scope.org = ".&ALLMEMBERS";
                if ($scope.dat == "")
                    $scope.dat = ".&ALLMEMBERS";
                var send = ' {"Date":"' + $scope.dat + '", "OrgRequest":"' + $scope.org + '"}';
                $q.all($scope.fullLinks.map(function (url) {
                    return $http.post(url, { id: send }).success(function (data) {
                        if (data.rowData.length > 0 && data.columnData.length > 0)
                            $scope.infos.push({ key: data.rowData[0][0], value: data.columnData[0] });
                        else
                            $scope.infos.push({ key: 0, value: 0 });

                    });
                })).then(function () {
                    for (var i = 0; i < $scope.dashChart.length; i++) {
                        for (var j = 0; j < $scope.infos.length; j++) {
                            if ($scope.dashChart[i].BiQueryName == $scope.infos[j].value) {
                                if ($scope.dashChart[i].BiQueryResultFormat == "1" && $scope.infos[j].key.indexOf('.') == -1)
                                    $scope.dashChart[i].perc = $scope.infos[j].key;
                                else if ($scope.dashChart[i].BiQueryResultFormat == "2")
                                    $scope.dashChart[i].perc = parseInt(parseFloat($scope.infos[j].key) * 100) + "%";
                                else if ($scope.dashChart[i].BiQueryResultFormat == "2" && $scope.infos[j].key == "0")
                                    $scope.dashChart[i].perc = $scope.infos[j].key + "%";

                                break;
                            }
                        }
                        $scope.dashChart[i].background = { 'background-color': $scope.Colors[i] };
                    }
                });
            })
        }

    }
})();