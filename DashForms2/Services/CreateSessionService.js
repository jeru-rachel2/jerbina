﻿angular.module('BlurAdmin.theme.components')
     .factory('CreateSessionService', CreateSessionService);  
function CreateSessionService($http) {
    var vm = this; 
    vm.get = function (id) {
       //      return $http.get('/dashboard/api/ApiCreateSession/' + id.id).success(function (response) {
       return $http.get('/api/ApiCreateSession/' + id.id).success(function (response) {
            return response;

        });
    }; 
    return { 
        get: vm.get,
    } 
}
CreateSessionService.$inject = ['$http'];