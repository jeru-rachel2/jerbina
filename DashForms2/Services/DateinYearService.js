﻿angular.module('BlurAdmin.pages.charts.chartist').factory('DateinYearService', DateinYearService);
function DateinYearService($resource, a) {
  return $resource('/api/ApiDateinYear/:id', {
        //   return $resource('/dashboard/api/ApiDateinYear/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DateinYearService.$inject = ['$resource'];
