﻿angular.module('BlurAdmin.pages.charts.chartist').factory('StartService', StartService);
function StartService($resource, a) {
   return $resource('/api/ApiStart/:id', {
  //     return $resource('/dashboard/api/ApiStart/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
StartService.$inject = ['$resource'];



