﻿angular.module('BlurAdmin.pages.charts.chartist').factory('GeneralReportService', GeneralReportService);
function GeneralReportService($resource, a) {
        return $resource('/api/ApiGeneralReport/:id', {
       // return $resource('/dashboard/api/ApiGeneralReport/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
GeneralReportService.$inject = ['$resource'];