﻿angular.module('BlurAdmin.pages.charts.chartist').factory('GuiScreenElementService', GuiScreenElementService);
function GuiScreenElementService($resource, a) {
     return $resource('/api/ApiGuiScreen/:id', {
 //  return $resource('/dashboard/api/ApiGuiScreen/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
GuiScreenElementService.$inject = ['$resource'];