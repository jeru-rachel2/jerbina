﻿angular.module('BlurAdmin.pages.charts.chartist').factory('DashboardService', DashboardService);
function DashboardService($resource, a) {
       return $resource('/api/ApiDashboard/:id', {
     // return $resource('/dashboard/api/ApiGeneralReport/:id', {
        id: '@id',
    }, {
        post: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE',
        },
    }, a);
}
DashboardService.$inject = ['$resource'];