/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.charts.chartist').controller('chartJs1DCtrl', chartJs1DCtrl);

    /** @ngInject */
    function chartJs1DCtrl($scope, baConfig, Translate) {
        var layoutColors = baConfig.colors;
        $scope.labels = [Translate('Status1'), Translate('Status2'), Translate('Status3')];
        $scope.labels2 = [Translate('Label12a'), Translate('Label1a'), Translate('Label15a'), Translate('Label14a'), Translate('Label13a')];
        $scope.labels3 = [Translate('Label14b'), Translate('Label1b'), Translate('Label13b')];
        $scope.labels4 = [Translate('Label16b'), Translate('Label17b'), Translate('Label18b')];
        //$scope.labels = [Translate('Status1'), Translate('Status2'), Translate('Status3'), Translate('Status4'), Translate('Status5')];
        $scope.data = [54, 17, 23];
        $scope.data2 = [10, 11, 11, 33, 64];
        $scope.data3 = [2, 1, 2];
        $scope.data4 = [200, 4, 35];
        //$scope.data = [20, 40, 5, 35, 12];
        $scope.options = {
            height: '300px',
            elements: {
                labelInterpolationFnc: function (value) {
                    return  value + '%';
                },
                arc: {
                    borderWidth: 0
                }
            },
            legend: {
                display: true,
                position: 'left',
                labels: {
                    fontColor: layoutColors.defaultText
                }
            },
            labelInterpolationFnc: function (value) {
                return  value + '%';
            }
        };

        $scope.changeData = function () {
            $scope.data = shuffle($scope.data);
        };

        function shuffle(o) {
            for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) { }
            return o;
        }
    }

})();