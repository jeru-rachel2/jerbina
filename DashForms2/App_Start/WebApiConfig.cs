﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DashForms2
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{type}",
                defaults: new { id = RouteParameter.Optional, type= RouteParameter.Optional }
            );
           

        }
    }
}
